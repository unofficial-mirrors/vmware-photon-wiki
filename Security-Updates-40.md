# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0015  
Type : Security  
Severity : [ Critical,Moderate ]  
Issued on : 2017-May-08  
Affected versions : PhotonOS 1.0  

## Details
An update of [freetype2,tar,gnutls,linux] packages for PhotonOS has been released.

## Affected Packages:

### [Critical]
freetype2 - [CVE-2017-7857](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7857) [CVE-2017-7858](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7858) [CVE-2017-7864](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7864)  

### [Moderate]
tar - [CVE-2016-6321](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-6321)   
gnutls - [CVE-2016-7444](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7444)      
linux - [CVE-2017-7889](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7889)  


## Solution
Install the update packages ( tdnf update package_name )   
Note: For package [ linux ] after updating, a reboot is required for taking effect.  


## Updated Packages Information
freetype2-2.7.1-2.ph1.x86_64.rpm , sha256 : 26870737543337994a80a8975f986f53bccc3276eb5e0c6aa41b96dfc2f2f370 , size : 328K , date : 2017-05-08   
freetype2-debuginfo-2.7.1-2.ph1.x86_64.rpm , sha256 : 780bd7b6f4f6910416cde0a2d6631871431a20f99176e82f5bbab8df26ebe776 , size : 1.8M , date : 2017-05-08   
freetype2-devel-2.7.1-2.ph1.x86_64.rpm , sha256 : 0f722a22d5ca144745fc1784948a2d0dd7f303b6c0c2003be5b888e36ac55067 , size : 179K , date : 2017-05-08   
tar-1.29-1.ph1.x86_64.rpm , sha256 : 1c1f90b55d4349aabd0e623524a519c87cf45d654a0a6341949b70a50bf8171b , size : 1.3M , date : 2017-05-08   
tar-debuginfo-1.29-1.ph1.x86_64.rpm , sha256 : f51ca5a2ca86b492d7df362bfe3e6aa26c15604e994a388b6d62a7a2e32c51b5 , size : 979K , date : 2017-05-08   
gnutls-3.4.11-3.ph1.x86_64.rpm , sha256 : 2770f3c7193671d3605565105a50f188fc6a3e52d0ba472b2ac9c1db005e9eac , size : 1.8M , date : 2017-05-08   
gnutls-debuginfo-3.4.11-3.ph1.x86_64.rpm , sha256 : 931d9616b209b1839c313eb9d6f39d53ad7d1d1714b20b59525cf8b5feb323e3 , size : 3.0M , date : 2017-05-08   
gnutls-devel-3.4.11-3.ph1.x86_64.rpm , sha256 : c38ac9e05cbc10833cee0b99c30cb91a0877d9cbc599f880e6f69fa99e570903 , size : 54K , date : 2017-05-08   
linux-api-headers-4.4.65-1.ph1.noarch.rpm , sha256 : b2e23d189b9acef018faa48b4650e548fdd58d4b93633357d7f3d527a932815a , size : 1.1M , date : 2017-05-08   
linux-4.4.65-1.ph1.x86_64.rpm , sha256 : 353710ed049397c832a4e87bf361183385951292a055ce8afd5e874e63ad70c1 , size : 17M , date : 2017-05-08   
linux-debuginfo-4.4.65-1.ph1.x86_64.rpm , sha256 : 9cf3d5012aac29b7395c1e1fc28983d1bc211173853d19b13c72086b8842f23b , size : 340M , date : 2017-05-08   
linux-dev-4.4.65-1.ph1.x86_64.rpm , sha256 : 9680b2198549002771d6c617c53e220803000c17bdc7f0b7a478a28da1d1536d , size : 9.9M , date : 2017-05-08   
linux-docs-4.4.65-1.ph1.x86_64.rpm , sha256 : fb4a3d6d4d6d59deff086d69917149db61de01ff63f87b8abba6841622035e6d , size : 6.6M , date : 2017-05-08   
linux-drivers-gpu-4.4.65-1.ph1.x86_64.rpm , sha256 : 484bcae21d5e31798bc9093aab4b6dab9d378655ac41ff5e2aed8fed14a1ccc9 , size : 1.4M , date : 2017-05-08   
linux-esx-4.4.65-1.ph1.x86_64.rpm , sha256 : 225579d456948a1a6583c8512888364a8825a5f2e0b547711717f0b7686fedd3 , size : 7.0M , date : 2017-05-08   
linux-esx-debuginfo-4.4.65-1.ph1.x86_64.rpm , sha256 : c41659eb7feb519006c95788efafafaaaf300b8dbd8abc0a94b282d6b8a8a10b , size : 154M , date : 2017-05-08   
linux-esx-devel-4.4.65-1.ph1.x86_64.rpm , sha256 : 78287b2718eec8f8b398f9d6adcf98e06553bcc022bf265a97cf4e2f0df45f66 , size : 9.7M , date : 2017-05-08    
linux-esx-docs-4.4.65-1.ph1.x86_64.rpm , sha256 : 0493f57f7869d992ede2cebc6a207613b0703129e50ab23cc0ece10cd8ddd709 , size : 6.6M , date : 2017-05-08     
linux-oprofile-4.4.65-1.ph1.x86_64.rpm , sha256 : e385c3ef79fa3672022252f05ba3e476a488379ecb3a81d69a67ec7fce502861 , size : 31K , date : 2017-05-08    
linux-sound-4.4.65-1.ph1.x86_64.rpm , sha256 : 47e69bdd19a66c7553641a4ca3e64b9146aa856818de5e80c9ff8babe9f280ba , size : 218K , date : 2017-05-08     
linux-tools-4.4.65-1.ph1.x86_64.rpm , sha256 : e39e0316543647b04043c604f6b3a7ba6225d37827bc975fb2102443d8a2877e , size : 687K , date : 2017-05-08     
