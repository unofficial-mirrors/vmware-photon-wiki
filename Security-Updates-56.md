# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0026   
Type : Security   
Severity : [ Critical,Moderate,Low ]  
Issued on : 2017-Jul-23  
Affected versions : PhotonOS 1.0   

## Details
An update of [openjdk,openjre,pycrypto,python3-pycrypto] packages for PhotonOS has been released.   

## Affected Packages:
### [Critical]
openjdk - [CVE-2013-7459 ](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2013-7459 )[CVE-2017-10089](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10089) [CVE-2017-10086](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10086) [CVE-2017-10096](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10096) [CVE-2017-10087](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10087) [CVE-2017-10090](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10090) [CVE-2017-10111](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10111) [CVE-2017-10107](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10107) [CVE-2017-10102](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10102) [CVE-2017-10114](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10114) [CVE-2017-10074](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10074) [CVE-2017-10116](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10116) [CVE-2017-10078](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10078) [CVE-2017-10067](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10067) [CVE-2017-10115](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10115) [CVE-2017-10118](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10118) [CVE-2017-10176](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10176) [CVE-2017-10104](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10104) [CVE-2017-10145](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10145) [CVE-2017-10125](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10125)    
openjre - [CVE-2013-7459 ](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2013-7459 )[CVE-2017-10089](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10089) [CVE-2017-10086](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10086) [CVE-2017-10096](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10096) [CVE-2017-10087](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10087) [CVE-2017-10090](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10090) [CVE-2017-10111](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10111) [CVE-2017-10107](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10107) [CVE-2017-10102](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10102) [CVE-2017-10114](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10114) [CVE-2017-10074](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10074) [CVE-2017-10116](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10116) [CVE-2017-10078](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10078) [CVE-2017-10067](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10067) [CVE-2017-10115](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10115) [CVE-2017-10118](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10118) [CVE-2017-10176](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10176) [CVE-2017-10104](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10104) [CVE-2017-10145](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10145) [CVE-2017-10125](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10125)    
pycrypto - [CVE-2013-7459](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2013-7459)  

### [Moderate]
openjdk - [CVE-2017-10198](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10198) [CVE-2017-10243](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10243) [CVE-2017-10121](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10121)    
openjre - [CVE-2017-10198](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10198) [CVE-2017-10243](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10243) [CVE-2017-10121](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10121)  

### [Low]
openjdk - [CVE-2017-10135](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10135) [CVE-2017-10117](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10117) [CVE-2017-10053](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10053) [CVE-2017-10108](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10108) [CVE-2017-10109](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10109) [CVE-2017-10105](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10105)   
openjre - [CVE-2017-10135](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10135) [CVE-2017-10117](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10117) [CVE-2017-10053](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10053) [CVE-2017-10108](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10108) [CVE-2017-10109](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10109) [CVE-2017-10105](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10105)  

## Solution
Install the update packages ( tdnf update package_name )



## Updated Packages Information
openjdk-1.8.0.141-1.ph1.x86_64.rpm , sha256 : 7a35c7c15ffb2ccd4842b5d83f8b60c82116e951d0d871ad0bd554b4ec214083 , size : 12M , date : 2017-07-23    
openjdk-debuginfo-1.8.0.141-1.ph1.x86_64.rpm , sha256 : 8092cdcbcab8b11d93ce9abbe4db3720e43bd361d72e0de6cab81090aa341d0f , size : 2.2M , date : 2017-07-23   
openjdk-doc-1.8.0.141-1.ph1.x86_64.rpm , sha256 : 409c2a9d6d8ddf35c692a9fad5ba2a84906531462d0a96612d6ce9dd179a6d57 , size : 2.3M , date : 2017-07-23   
openjdk-sample-1.8.0.141-1.ph1.x86_64.rpm , sha256 : 62ccb7776a879d9dfe4094dfe044692245681db279fc6bc1ea7b5ec32e9c17e1 , size : 449K , date : 2017-07-23   
openjdk-src-1.8.0.141-1.ph1.x86_64.rpm , sha256 : ab5e5478a5bedb4a876a0025cd8e37633f144eb38b3fd2fcf826909798b1cf72 , size : 22M , date : 2017-07-23    
openjre-1.8.0.141-1.ph1.x86_64.rpm , sha256 : 274e1fcb1ea756ff70b28a23632c848217f67b1ea1b8b3d3b507b69b0bad6468 , size : 40M , date : 2017-07-23   
pycrypto-2.6.1-3.ph1.x86_64.rpm , sha256 : 8b6dc33001caeec195db324771e7437b75a1f009207ed639aa41712b1111d0f6 , size : 663K , date : 2017-07-23    
pycrypto-debuginfo-2.6.1-3.ph1.x86_64.rpm , sha256 : e7b5ff861f4a38d2d12006c94a4886915d2b86e7033aecb702ec7c219be01cb6 , size : 36K , date : 2017-07-23   
python3-pycrypto-2.6.1-3.ph1.x86_64.rpm , sha256 : 4947e04e258a587d0da3a384ce747401c4217a4bd2663fd0a1cbb2eeba9b7d55 , size : 711K , date : 2017-07-23   
