# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0003   
Type : Security   
Severity : [ Critical,Moderate,Low ]   
Issued on : 2017-Jan-27   
Affected versions : PhotonOS 1.0    

## Details
An update of [guile,ntp] packages for PhotonOS has been released.    

## Affected Packages:

### [Critical]   
guile - [CVE-2016-8606](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8606)     

### [Moderate]   
ntp - [CVE-2016-9310](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-9310)     

### [Low]
ntp - [CVE-2016-7431](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7431)     
ntp - [CVE-2016-9311](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-9311)    
ntp - [CVE-2016-7427](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7427)    
ntp - [CVE-2016-7428](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7428)    
ntp - [CVE-2016-7434](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7434)    
ntp - [CVE-2016-7429](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7429)    
ntp - [CVE-2016-7426](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7426)   
ntp - [CVE-2016-7433](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7433)    

## Solution
Install the update packages ( _tdnf update package_name_ )      



## Updated Packages Information
guile-2.0.13-1.ph1.x86_64.rpm , sha256 : 97a98f2a6f42515228faddee42a581e1408be895958be9de1b702887d8f0297d , size : 3.2M , date : 2017-01-27    
guile-debuginfo-2.0.13-1.ph1.x86_64.rpm , sha256 : 99245f887e9bfb71649939437a24e8cf590691db7f26c4c44807f5aa21665fbf , size : 2.2M , date : 2017-01-27    
guile-devel-2.0.13-1.ph1.x86_64.rpm , sha256 : 152b77618f09d9df5899a9a0bb72dd234b551470176eb8e643d892934a17174b , size : 116K , date : 2017-01-27    
ntp-4.2.8p9-1.ph1.x86_64.rpm , sha256 : 56f818e6fc7d63a5f583478c7c4edb0a0a818aa6c644df3b53b03f497746ba02 , size : 3.5M , date : 2017-01-27    
ntp-debuginfo-4.2.8p9-1.ph1.x86_64.rpm , sha256 : 3c2590556ff64f0b6db1f168e4d55d816037612e1b8be4a6cfba770d43454970 , size : 3.6M , date : 2017-01-27   
ntpstat-4.2.8p9-1.ph1.x86_64.rpm , sha256 : 052768d80723a134c25fae765b65d970f5d0277e110b2bda039d62114d549705 , size : 7.8K , date : 2017-01-27   