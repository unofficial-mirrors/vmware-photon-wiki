# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0019   
Type : Security  
Severity : [Critical,Low]   
Issued on : 2017-Jun-14   
Affected versions : PhotonOS 1.0   

## Details
An update of [linux] packages for PhotonOS has been released.  
## Solution

Install the update packages ( tdnf update package_name )   
Note: For package [ linux ] after updating, a reboot is required for taking effect.   

## Affected Packages:
### [Critical]
linux - [CVE-2017-8890](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-8890) [CVE-2017-9074](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9074) [CVE-2017-9075](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9075) [CVE-2017-9076](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9076)

### [Low]
linux - [CVE-2017-9242](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9242)  

## Updated Packages Information
linux-api-headers-4.4.71-1.ph1.noarch.rpm , sha256 : ef20e6b40264edb1e16358f4a57ebbed8ab288c552624b23066c18a8c746f259 , size : 1.1M , date : 2017-06-14   
linux-4.4.71-1.ph1.x86_64.rpm , sha256 : d6f4faa7ff2e82ac89e6684229c22217394e7386aac93c8f622d2ad94440ae6e , size : 18M , date : 2017-06-14   
linux-debuginfo-4.4.71-1.ph1.x86_64.rpm , sha256 : 490165df8c7415ef9e3dda82b6a732b7beb3fcc434ef97eefc669e741415600e , size : 341M , date : 2017-06-14  
linux-dev-4.4.71-1.ph1.x86_64.rpm , sha256 : 214bed350c5406b1806db27f954b9f714c8dd72e54261522f01fd542de364c95 , size : 9.9M , date : 2017-06-14  
linux-docs-4.4.71-1.ph1.x86_64.rpm , sha256 : 0b488293940c6dbc8924ede2b0aa9af423d226509a1c9b45749d8f7f15b90057 , size : 6.6M , date : 2017-06-14   
linux-drivers-gpu-4.4.71-1.ph1.x86_64.rpm , sha256 : c0f343d98812ad26d96399eb695b68a6284f885b3d51b575ff97c6514ed32ba1 , size : 1.4M , date : 2017-06-14  
linux-esx-4.4.71-1.ph1.x86_64.rpm , sha256 : ba6c3e67720a1ad356ca340d9d5d3bbfa2ca6225ff85b34cc6c034f01016615f , size : 7.1M , date : 2017-06-14   
linux-esx-debuginfo-4.4.71-1.ph1.x86_64.rpm , sha256 : a2e0e630d04584df7db007ea01cc1476cddc693102d924e29ab2b19d52b99c30 , size : 155M , date : 2017-06-14   
linux-esx-devel-4.4.71-1.ph1.x86_64.rpm , sha256 : 7cdc1345bd7d76753d02a2b9a93b7a77c4f73ad9a3bccc716bbb16719ba27710 , size : 9.7M , date : 2017-06-14   
linux-esx-docs-4.4.71-1.ph1.x86_64.rpm , sha256 : 60bf4ce2f653ddf121914e59752819dc743b6ae51a4a42778bb262950b0d36ae , size : 6.6M , date : 2017-06-14   
linux-oprofile-4.4.71-1.ph1.x86_64.rpm , sha256 : 1d9484341cb0877fa5334d8fe5de49349797610bd4dffff094c639e2ebe6e511 , size : 32K , date : 2017-06-14   
linux-sound-4.4.71-1.ph1.x86_64.rpm , sha256 : c8ba72594fed80c0981cc1d7ab5fe7cd6b6a0c6d5e9a0bccab6b9f30c364f383 , size : 226K , date : 2017-06-14   
linux-tools-4.4.71-1.ph1.x86_64.rpm , sha256 : c4fe1653b0acebaab3a660ef