# [Moderate] Photon OS Security Update


## Summary   
Advisory ID: PHSA-2017-0012   
Type : Security   
Severity : [Moderate]   
Issued on : 2017-Apr-20   
Affected versions : PhotonOS 1.0   

## Details
An update of [apache-tomcat] package for PhotonOS has been released.  

Affected Packages:

### [Moderate]
apache-tomcat - [CVE-2016-8747](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8747)

## Solution
Install the update packages ( tdnf update package_name )

## Updated Packages Information
apache-tomcat-8.5.13-1.ph1.noarch.rpm , sha256 : 49f64e7661151b3d1f1af10d6f9a708b6c1cdd477636cf9b30f981be72953c8d , size : 7.8M , date : 2017-04-20   
