![Photon](https://cloud.githubusercontent.com/assets/11306358/9800286/cb4c9eb6-57d1-11e5-916c-6eba8e40fa99.png)
# Welcome to the Photon OS Wiki

This wiki serves as an unofficial supplement to the documentation that is published in the project .md files. 


### Photon OS 2.0 BETA Available!

Photon OS 2.0 introduces new security and OS management capabilities, along with new and updated packages for cloud-native applications and VMware appliances. To download the beta files, go to [Downloading Photon OS](https://github.com/vmware/photon/wiki/Downloading-Photon-OS).

# Table of Contents

1. [Frequently Asked Questions](https://github.com/vmware/photon/wiki/Frequently-Asked-Questions)
2. Getting Started Guides
    * [Downloading Photon OS](https://github.com/vmware/photon/wiki/Downloading-Photon-OS)
    * [Running Photon OS on vSphere](https://github.com/vmware/photon/wiki/Running-Photon-OS-on-vSphere)
    * [Running Photon OS on Fusion](https://github.com/vmware/photon/wiki/Running-Project-Photon-on-Fusion)
    * [Running Photon OS on vCloud Air](https://github.com/vmware/photon/wiki/Running-Project-Photon-on-vCloud-Air)
    * [Running Photon OS on Google Compute Engine](https://github.com/vmware/photon/wiki/Running-Photon-OS-on-Google-Compute-Engine)

3. How-To Guides
    * [Install and Configure a Swarm Cluster with DNS Service on Photon OS](https://github.com/vmware/photon/wiki/Install-and-Configure-a-Swarm-Cluster-with-DNS-Service-on-PhotonOS)
    * [Install and Configure a Production Ready Mesos Cluster on Photon OS](https://github.com/vmware/photon/wiki/Install-and-Configure-a-Production-Ready-Mesos-Cluster-on-Photon-OS)
    * [Install and Configure Marathon for Mesos Cluster on Photon OS](https://github.com/vmware/photon/wiki/Install-and-Configure-Marathon-for-Mesos-Cluster-on-PhotonOS)
    * [Install and Configure DCOS CLI for Mesos](https://github.com/vmware/photon/wiki/Install-and-Configure-DCOS-CLI-for-Mesos)
    * [Install and Configure Mesos DNS on a Mesos Cluster](https://github.com/vmware/photon/wiki/Install-and-Configure-Mesos-DNS-on-a-Mesos-Cluster)
 
4. [Photon OS Administration Guide](https://github.com/vmware/photon/blob/master/docs/photon-admin-guide.md)