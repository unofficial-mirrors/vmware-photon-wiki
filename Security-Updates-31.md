# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0008   
Type : Security   
Severity : [ Critical, Moderate ]    
Issued on : 2017-Mar-30   
Affected versions : PhotonOS 1.0   

## Details
An update of [xcerces-c,linux] packages for PhotonOS has been released.    

## Affected Packages:

### [Critical]
xcerces-c - [CVE-2016-2099](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-2099)    

### [Moderate]
linux - [CVE-2017-6346](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6346) [CVE-2017-6347](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6347) [CVE-2017-6874](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6874)

## Solution
Install the update packages (_ tdnf update package_name_ )    

## Updated Packages Information
linux-api-headers-4.4.54-1.ph1.noarch.rpm , sha256 : 09ff7fc4099d81cb924874bfe7048d082690e1244b09bb505840ac4cff572d38 , size : 1.1M , date : 2017-03-28     
linux-4.4.54-1.ph1.x86_64.rpm , sha256 : eb5566aec177b7e67362bf19eb96d0efb9bf0992cab9255fb6046f67f838bf39 , size : 17M , date : 2017-03-28    
linux-debuginfo-4.4.54-1.ph1.x86_64.rpm , sha256 : e54aec87b88965bf50bd560a8fc1b26c2ef680e2acf222164724f064b9ac833f , size : 336M , date : 2017-03-28    
linux-dev-4.4.54-1.ph1.x86_64.rpm , sha256 : c1547f5654b5a3467b65187dcea37cb44fbef1131e02c2e0d010fde815779b8b , size : 9.9M , date : 2017-03-28    
linux-docs-4.4.54-1.ph1.x86_64.rpm , sha256 : 91a4694f6431a9908a237d412bfd5ef5402487dc23441d4fb3367945fae1c3df , size : 6.6M , date : 2017-03-28    
linux-drivers-gpu-4.4.54-1.ph1.x86_64.rpm , sha256 : e014b99ca85aaeaddeb8a72fad1be017db769ac6df2a7a26833a37ebb8dea145 , size : 1.4M , date : 2017-03-28    
linux-esx-4.4.54-1.ph1.x86_64.rpm , sha256 : 34cad9c277224a0e45fe4764cef9a1e9fc29668b9335518690841c3371ba7801 , size : 7.0M , date : 2017-03-28    
linux-esx-debuginfo-4.4.54-1.ph1.x86_64.rpm , sha256 : 6c6f599f71ef1efdd1758f5ea2344551e1d5f5591d7986b4c1258f39b971021f , size : 4.1M , date : 2017-03-28     
linux-esx-4.4.54-1.ph1.x86_64.rpm , sha256 : 3a9aea60e30c8959f29f472a79ede68d954e172ffd6161b54fe81041684c70a4 , size : 9.7M , date : 2017-03-28     
linux-esx-docs-4.4.54-1.ph1.x86_64.rpm , sha256 : 4d312a7a5820567969eab5e62282d88a6087a4c4cd232617f114613112dcc13b , size : 6.6M , date : 2017-03-28    
linux-oprofile-4.4.54-1.ph1.x86_64.rpm , sha256 : 24e75bbae7875cbd93ec3807b26cb01b2b149d4a4ad083ad67940aaa788d78cc , size : 30K , date : 2017-03-28    
linux-sound-4.4.54-1.ph1.x86_64.rpm , sha256 : fa0f3bd93cbb31607ea0612467886d1830d3353853a57b06022788bf0a32f776 , size : 218K , date : 2017-03-28    
linux-tools-4.4.54-1.ph1.x86_64.rpm , sha256 : dfc90484250fecb9560e94fa7b48eb337ddbde2fa4b4b73b6993a9840d703060 , size : 704K , date : 2017-03-28   
linux-tools-debuginfo-4.4.54-1.ph1.x86_64.rpm , sha256 : ebe38640e6cf0e8067bc4e76e605cfd7347c82aadd90d54a3aa7d1d15ad3b27a , size : 3.4M , date : 2017-03-28   