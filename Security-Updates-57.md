# [Important] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0027   
Type : Security   
Severity : [Important]   
Issued on : 2017-Jul-25  
Affected versions : PhotonOS 1.0   

## Details
An update of [httpd] packages for PhotonOS has been released.

## Affected Packages:
### [Important]
httpd - [CVE-2017-3167](https://nvd.nist.gov/vuln/detail/CVE-2017-3167)

### [Moderate]
httpd - [CVE-2017-9788](https://nvd.nist.gov/vuln/detail/CVE-2017-9788)

## Solution
Install the update packages ( tdnf update package_name )


## Updated Packages Information
httpd-2.4.27-1.ph1.x86_64.rpm , sha256 : eaada53889ad0e7ae0f31b23b44017c763f986c83241f61485574dc7b545e19f , size : 1.7M , date : 2017-07-25   
httpd-debuginfo-2.4.27-1.ph1.x86_64.rpm , sha256 : cff6f9a5fb771624cd1540985cffba4c69817a77da9359ada76f4fc2a92a7c76 , size : 4.8M , date : 2017-07-25   
httpd-devel-2.4.27-1.ph1.x86_64.rpm , sha256 : a6de2ec371f9d53e483b6f0393d3d5202a9fb55001fde458147557a1c5b8279e , size : 184K , date : 2017-07-25    
httpd-docs-2.4.27-1.ph1.x86_64.rpm , sha256 : 2310c5b7451f4af053ccd83d35be1fdf6d586de2e445f9f5395101b4a2870a8d , size : 5.2M , date : 2017-07-25    
httpd-tools-2.4.27-1.ph1.x86_64.rpm , sha256 : 7c7319b4ac40ab2e7e6760de8ec0a8cff69a382bfe5507d3dd4bcb32cf5ead4f , size : 14K , date : 2017-07-25   
