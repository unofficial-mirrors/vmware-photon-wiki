# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0017    
Type : Security   
Severity : [Critical]    
Issued on : 2017-May-22   
Affected versions : PhotonOS 1.0   

## Details
An update of [rpcbind,libtirpc,freetype2] packages for PhotonOS has been released.   

## Affected Packages:

### [Critical]
rpcbind - [CVE-2017-8779](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-8779)   
libtirpc - [CVE-2017-8779](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-8779)   
freetype2 - [CVE-2017-8287](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-8287)   

## Solution
Install the update packages ( tdnf update package_name )

## Updated Packages Information
rpcbind-0.2.3-8.ph1.x86_64.rpm , sha256 : 13ef5e1386766b9e7d4bd4f2f4f3a548b3395f4bcdf1eb1758b57cc94f0856d4 , size : 45K , date : 2017-05-22   
rpcbind-debuginfo-0.2.3-8.ph1.x86_64.rpm , sha256 : c1fd9ecc15a670be62b802bedaa53b5ae25b9a8c51f80a41fef28660ea4c416b , size : 145K , date : 2017-05-22   
libtirpc-1.0.1-4.ph1.x86_64.rpm , sha256 : 46dd017e2d6800eef4293a0f88897684abbc02ff14a356a2b7eba231160ec7fc , size : 273K , date : 2017-05-22   
libtirpc-debuginfo-1.0.1-4.ph1.x86_64.rpm , sha256 : 1178a5ed4de02277e5533af09f3829163d47957bc46c359c5b1cd3ed68fb9aa6 , size : 469K , date : 2017-05-22   
libtirpc-devel-1.0.1-4.ph1.x86_64.rpm , sha256 : 9aa5218db3091df9a7f119000b368a29545f45086c6e7c3d5cee3c43343bc255 , size : 43K , date : 2017-05-22   
freetype2-2.7.1-3.ph1.x86_64.rpm , sha256 : eae00e2b1a049b66074a5441c27b3460bfbb02016ef66319c7f08aea38548336 , size : 328K , date : 2017-05-22   
freetype2-debuginfo-2.7.1-3.ph1.x86_64.rpm , sha256 : 64b7939a786a89b200c162fe1c17e40c406c604aa9f4b4220295d5deb1e28d8d , size : 1.8M , date : 2017-05-22   
freetype2-devel-2.7.1-3.ph1.x86_64.rpm , sha256 : 2ebb3c87cad55338252f39807d139e94fbb252d82dae2dc19068189176ef69d4 , size : 179K , date : 2017-05-22   
