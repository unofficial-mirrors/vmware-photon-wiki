# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0022   
Type : Security   
Severity : [Critical,Moderate]  
Issued on : 2017-Jun-30   
Affected versions : PhotonOS 1.0  

## Details
An update of [linux,glibc] packages for PhotonOS has been released.    

## Affected Packages:

### [Critical]
linux - [CVE-2017-1000364](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-1000364)

### [Moderate]
linux - [CVE-2017-9605](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9605)   
glibc - [CVE-2017-1000366](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-1000366)

## Solution
Install the update packages ( tdnf update package_name )    
Note: For package [ linux ] after updating, a reboot is required for taking effect.

## Updated Packages Information
linux-api-headers-4.4.74-1.ph1.noarch.rpm , sha256 : 38856fc91c55bb4f535fe9c491d53a1020958daea8d681ebd735a9689c889e6f , size : 1.1M , date : 2017-06-30    
linux-4.4.74-1.ph1.x86_64.rpm , sha256 : 3e2f51001816b3c8833189dd522e0c1421fa892e2c290a6686916156aecff2d4 , size : 18M , date : 2017-06-30   
linux-debuginfo-4.4.74-1.ph1.x86_64.rpm , sha256 : bbac07c056e8cecb27f5ec4d06ee2ff68bedef5a9da9a9ddc2c38db913d94dd2 , size : 345M , date : 2017-06-30   
linux-dev-4.4.74-1.ph1.x86_64.rpm , sha256 : fa229bce5920eda7e7fe2fc4b8813a4df05ad04d730d38844771703879ccadbc , size : 9.9M , date : 2017-06-30   
linux-docs-4.4.74-1.ph1.x86_64.rpm , sha256 : dbdd0e3f2765b35892ba2bb673844a3f2706331bac3bd736a8e3615b5fbfa316 , size : 6.6M , date : 2017-06-30   
linux-drivers-gpu-4.4.74-1.ph1.x86_64.rpm , sha256 : 90716f6bda0fcdc68dce1728c5ae77d953b9d0478b354b957ed6904d806e8ea3 , size : 1.4M , date : 2017-06-30  
linux-esx-4.4.74-1.ph1.x86_64.rpm , sha256 : bdefd4440a888822c27c98e7539bc0e30de0384cace1ebe40effe6fd6f2afdd2 , size : 7.1M , date : 2017-06-30   
linux-esx-debuginfo-4.4.74-1.ph1.x86_64.rpm , sha256 : 5de47d3c9423660b8e964e2fba1ea272ea98eb99e3431d5bfa32ad77d8006cb0 , size : 155M , date : 2017-06-30    
linux-esx-devel-4.4.74-1.ph1.x86_64.rpm , sha256 : 8653d520a72e2b9eb37110d07077d03f72e65604af3f8abd586655849d06ffe0 , size : 9.7M , date : 2017-06-30   
linux-esx-docs-4.4.74-1.ph1.x86_64.rpm , sha256 : 6a472b803799b4ecb5d1fb9be6a71ca0d1415850bfa22ae7f6f689f8b5133e4a , size : 6.6M , date : 2017-06-30    
linux-oprofile-4.4.74-1.ph1.x86_64.rpm , sha256 : 9e342ac10ce68ef6dd982355e7c0a66c170b7af762cba7789e617f06024c7a11 , size : 32K , date : 2017-06-30    
linux-sound-4.4.74-1.ph1.x86_64.rpm , sha256 : 2b448f84b298b65b41c128ad258d5483ce91f831432d8def48f493fa1a5e725f , size : 227K , date : 2017-06-30   
linux-tools-4.4.74-1.ph1.x86_64.rpm , sha256 : 0c8c973875da9190ecdeb0e884d3d1e78f891c80d72223c50a24544c63483de7 , size : 729K , date : 2017-06-30    
glibc-2.22-12.ph1.x86_64.rpm , sha256 : f385332da7bac4f3ec876afddcc3a60e6e5017e2e8fc594e7f5eec164eb0645d , size : 19M , date : 2017-06-30    
glibc-devel-2.22-12.ph1.x86_64.rpm , sha256 : f4d8664adb52f074afba1f23da12b98d5e5aacc7504897b365ac878571614d5c , size : 9.8M , date : 2017-06-30    
glibc-lang-2.22-12.ph1.x86_64.rpm , sha256 : a2d5c9ebe8ab72011d764cb2298a13dd432cdc3fbbaf9dde4ccc8d0b9b647bb0 , size : 1.4M , date : 2017-06-30   
