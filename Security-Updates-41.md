# [Moderate] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0016   
Type : Security   
Severity : [Moderate]   
Issued on : 2017-May-11   
Affected versions : PhotonOS 1.0   

## Details
An update of [gnutls,openjdk,openjre] packages for PhotonOS has been released.

## Affected Packages:

## [Moderate]
linux - [CVE-2017-7889](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7889)    
gnutls - [CVE-2017-7869](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7869)   
openjdk - [CVE-2016-5546](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5546) [CVE-2016-5547](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5547) [CVE-2016-5548](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5548) [CVE-2016-5549](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5549) [CVE-2016-5552](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5552) [CVE-2017-3231](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3231) [CVE-2017-3253](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3253) [CVE-2017-3261](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3261) [CVE-2016-8328](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8328) [CVE-2017-3259](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3259) [CVE-2017-3260](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3260) [CVE-2017-3262](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3262) [CVE-2017-3241](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3241) [CVE-2017-3289](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3289) [CVE-2017-3272](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3272)   
openjre - [CVE-2016-5546](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5546) [CVE-2016-5547](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5547) [CVE-2016-5548](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5548) [CVE-2016-5549](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5549) [CVE-2016-5552](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5552) [CVE-2017-3231](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3231) [CVE-2017-3253](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3253) [CVE-2017-3261](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3261) [CVE-2016-8328](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8328) [CVE-2017-3259](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3259) [CVE-2017-3260](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3260) [CVE-2017-3262](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3262) [CVE-2017-3241](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3241) [CVE-2017-3289](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3289) [CVE-2017-3272](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3272)  

## Solution
Install the update packages ( _tdnf update package_name_ )

## Updated Packages Information
linux-api-headers-4.4.67-1.ph1.noarch.rpm , sha256 : 4566ae7f411da249ac05e25427834aa9d52c003031fc99c3cb13ece36c299309 , size : 1.1M , date : 2017-05-11    
linux-4.4.67-1.ph1.x86_64.rpm , sha256 : 82fb1bc1b036be4628b70c2f0403c61f69ccc0406c8e3c3e0902016f7b458ebd , size : 17M , date : 2017-05-11   
linux-debuginfo-4.4.67-1.ph1.x86_64.rpm , sha256 : 5105d26f14431598c48c14664cb96c68259ca979a7e9246b2eca67f00c35569a , size : 340M , date : 2017-05-11   
linux-dev-4.4.67-1.ph1.x86_64.rpm , sha256 : a87f7c55f152a7f589de03c6c1f0e1c93d6f9733cf30bc455b337be28a074a12 , size : 9.9M , date : 2017-05-11    
linux-docs-4.4.67-1.ph1.x86_64.rpm , sha256 : 642d7e2290e832d74b147502d527161f7d128b04c46f5eecaa6cd6521d19de05 , size : 6.6M , date : 2017-05-11   
linux-drivers-gpu-4.4.67-1.ph1.x86_64.rpm , sha256 : 4dde8218c7954b9bd292d9c665761b9d645da43efdf642cce3c926ab4da10a02 , size : 1.4M , date : 2017-05-11   
linux-esx-4.4.67-1.ph1.x86_64.rpm , sha256 : 64f6aac64a78b16505c352c1ad3809d25484f876e0e2662cd9c1edbd8f61e678 , size : 7.0M , date : 2017-05-11      
linux-esx-debuginfo-4.4.67-1.ph1.x86_64.rpm , sha256 : e25e1988df8c0f227c7cdd6c5309f235d0634637bf304438ed6eedb0e970abf6 , size : 154M , date : 2017-05-11      
linux-esx-devel-4.4.67-1.ph1.x86_64.rpm , sha256 : e2cbdbbb5f3dc72e6d3e9236b84fa95a3b81bf4b721753b2603a1c5621386c7d , size : 9.7M , date : 2017-05-11       
linux-esx-docs-4.4.67-1.ph1.x86_64.rpm , sha256 : ef29ab7e6f0f8d15c83d20bcb9b1d0a830ae489f0b61d41aae1ca59368cc3806 , size : 6.6M , date : 2017-05-11      
linux-oprofile-4.4.67-1.ph1.x86_64.rpm , sha256 : cebff9d8fbafb7f7c6cef510288ffa6c062fcc1dbdd6ec7af0df21651a566631 , size : 32K , date : 2017-05-11      
linux-sound-4.4.67-1.ph1.x86_64.rpm , sha256 : 2b30c1ea3c9bb9777cf6ad1d0d15a1c82ef626949d79affb31048f94aa12850d , size : 226K , date : 2017-05-11   
linux-tools-4.4.67-1.ph1.x86_64.rpm , sha256 : d03f7432e17b283bbb4c40442764c8e3d08c3d0e6a0ba9a9232030fe43b90baa , size : 687K , date : 2017-05-11   
gnutls-3.4.11-4.ph1.x86_64.rpm , sha256 : 656bf4b4b16c9c394bdfff877abf98a6bd9f07610a208c4932f2eb02a1988439 , size : 1.8M , date : 2017-05-11   
gnutls-debuginfo-3.4.11-4.ph1.x86_64.rpm , sha256 : a7586d957eb772b5a9146507d6a9fb04c23e11d75e42ce6042773dd26a6257e5 , size : 3.0M , date : 2017-05-11   
gnutls-devel-3.4.11-4.ph1.x86_64.rpm , sha256 : b0d12b5a69ec9cf948bfb0961c4ee973101a670ace5b9e1f2a929cac6fdd43c3 , size : 54K , date : 2017-05-11   
openjdk-1.8.0.131-1.ph1.x86_64.rpm , sha256 : 5094157a29d38e19de2d9304d546ef6151382aa99c0acd1f9947535e1606d937 , size : 12M , date : 2017-05-11    
openjdk-debuginfo-1.8.0.131-1.ph1.x86_64.rpm , sha256 : 38f383c2d6a241aa0c1f11a86ac98d9544883edad2983d5cd9b590deb8cd380e , size : 2.2M , date : 2017-05-11    
openjdk-doc-1.8.0.131-1.ph1.x86_64.rpm , sha256 : 0c42d4e6f0ef028b39ae51e1159b3e764b4197148971cf2c2f83e7a4c881dff8 , size : 2.3M , date : 2017-05-11   
openjdk-sample-1.8.0.131-1.ph1.x86_64.rpm , sha256 : f9f719200e02c78e8c33f342b7994ee016cf447ba3133cac14dcc343f2e331c6 , size : 449K , date : 2017-05-11   
openjdk-src-1.8.0.131-1.ph1.x86_64.rpm , sha256 : 62a7d65f3417cc8b9925605f2596092960aba13f75ef14d8de7670062e2f07ac , size : 22M , date : 2017-05-11    
openjre-1.8.0.131-1.ph1.x86_64.rpm , sha256 : 26347c0f541a26fa76bd9d0d531845c3d46fed6760ecc8a9a2798535033561c6 , size : 158M , date : 2017-05-11   
