# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0004   
Type : Security   
Severity : [ Critical ]   
Issued on : 2017-Feb-10   
Affected versions : PhotonOS 1.0   

## Details

An update of [tcpdump] packages for PhotonOS has been released.

## Affected Packages:

### [Critical]   
tcpdump - [CVE-2016-7922](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7922) [CVE-2016-7923](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7923) [CVE-2016-7924](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7924) [CVE-2016-7925](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7925) [CVE-2016-7926](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7926) [CVE-2016-7927](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7927) [CVE-2016-7928](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7928) [CVE-2016-7929](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7929) [CVE-2016-7930](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7930) [CVE-2016-7931](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7931) [CVE-2016-7932](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7932) [CVE-2016-7933](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7933) [CVE-2016-7934](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7934) [CVE-2016-7935](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7935) [CVE-2016-7936](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7936) [CVE-2016-7937](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7937) [CVE-2016-7938](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7938) [CVE-2016-7939](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7939) [CVE-2016-7940](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7940) [CVE-2016-7973](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7973) [CVE-2016-7974](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7974) [CVE-2016-7975](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7975) [CVE-2016-7983](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7983) [CVE-2016-7984](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7984) [CVE-2016-7985](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7985) [CVE-2016-7986](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7986) [CVE-2016-7992](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7992) [CVE-2016-7993](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7993) [CVE-2016-8574](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8574) [CVE-2016-8575](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8575) [CVE-2017-5202](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5202) [CVE-2017-5203](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5203) [CVE-2017-5204](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5204) [CVE-2017-5205](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5205) [CVE-2017-5341](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5341) [CVE-2017-5342](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5342) [CVE-2017-5482](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5482) [CVE-2017-5483](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5483) [CVE-2017-5484](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5484) [CVE-2017-5485](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5485) [CVE-2017-5486](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5486)


## Solution
Install the update packages ( _tdnf update package_name_ )


## Updated Packages Information
tcpdump-4.9.0-1.ph1.x86_64.rpm , sha256 : 9aed231c4902e2b9299bb7be0aec280a05fee018f947f209af27071d6ce63183 , size : 852K , date : 2017-02-10    
tcpdump-debuginfo-4.9.0-1.ph1.x86_64.rpm , sha256 : 306d0096bc06bebbbbada44dfb190fb77817a958ab5587426cae7873a6e9f369 , size : 2.3M , date : 2017-02-10   
