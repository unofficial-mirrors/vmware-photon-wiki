# [Important] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0029   
Type : Security   
Severity : [Important,Moderate,Low]   
Issued on : 2017-Aug-16  
Affected versions : PhotonOS 1.0   

## Details
An update of [ruby,cassandra,linux,libxml2] packages for PhotonOS has been released.   

## Affected Packages:
### [Important]
linux - [CVE-2017-1000112](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-1000112)  
ruby - [CVE-2017-9228](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9228)   
cassandra - [CVE-2017-3162](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3162)  

## [Moderate]
linux - [CVE-2017-7533](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7533)  
libxml2 - [CVE-2017-8872](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-8872)  

### [Low]
linux - [CVE-2017-7542](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7542) [CVE-2017-10911](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10911)   
cassandra - [CVE-2017-3161](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-3161)   

## Solution
Install the update packages ( tdnf update package_name )    
Note: For package [ linux ] after updating, a reboot is required for taking effect.     
## Updated Packages Information
ruby-2.4.0-5.ph1.x86_64.rpm , sha256 : 509339be5c262c89cc9fa3071d1c53a1c20c2191bfc4985e93564d2873a404ee , size : 12M , date : 2017-08-16  
ruby-debuginfo-2.4.0-5.ph1.x86_64.rpm , sha256 : f85a3cb6d16124b40f60badcbe8bcd753fe9c60e6f18d80e60dacc983e039b1d , size : 16M , date : 2017-08-16  
cassandra-3.10-5.ph1.x86_64.rpm , sha256 : 0dd7077d1aaf1fa31bba840936a6383badf45a3e64ecd98e6ca745766f20e172 , size : 72M , date : 2017-08-16  
linux-api-headers-4.4.82-1.ph1.noarch.rpm , sha256 : b9532d40e02615452fad3e216ef7f5f6fae9acfa47d6f424f132fb7145947e18 , size : 1.1M , date : 2017-08-16   
linux-4.4.82-1.ph1.x86_64.rpm , sha256 : ab973a17e520b7a200dde6d6731dfe82ac4e5a8dd05689300b4912dcba9eefb0 , size : 18M , date : 2017-08-16  
linux-debuginfo-4.4.82-1.ph1.x86_64.rpm , sha256 : df30b5a278ed25a08f054b2f79b4b918d92120216a5bf641571f6858b0b56f94 , size : 346M , date : 2017-08-16   
linux-dev-4.4.82-1.ph1.x86_64.rpm , sha256 : 4f2ee05fbeec1efd2c5c583efbd3dcc23c4d725e9d9f3b049e83e6e5213db9f1 , size : 10M , date : 2017-08-16    
linux-docs-4.4.82-1.ph1.x86_64.rpm , sha256 : 78c0e065cd39ea369d488ba835d0d31641ef0b8b9642539a7b1a5957d1e4e75d , size : 6.6M , date : 2017-08-16   
linux-drivers-gpu-4.4.82-1.ph1.x86_64.rpm , sha256 : 9e4a0f53899433345af614bf029ccaf9ef8a53c0176d2761f16d65e6d559df1c , size : 1.4M , date : 2017-08-16   
linux-esx-4.4.82-1.ph1.x86_64.rpm , sha256 : 4185b160a271df8966ab9dbcd52c41fcb6cf7a240c3568d49581927578433271 , size : 7.1M , date : 2017-08-16   
linux-esx-debuginfo-4.4.82-1.ph1.x86_64.rpm , sha256 : dee05cdcd5434b7df820006f238c945a8ed948638f9a3a0fffd6a6d4be403351 , size : 156M , date : 2017-08-16   
linux-esx-devel-4.4.82-1.ph1.x86_64.rpm , sha256 : cf5106a19a3eed5b4eb4ec3d3fdf34a9618e4000fb8b96a1cc66ca9349d99a8f , size : 9.7M , date : 2017-08-16   
linux-esx-docs-4.4.82-1.ph1.x86_64.rpm , sha256 : 60815938f835a4c78089dc182eae01e971c9573e43cd6bac116daf783dd1092b , size : 6.6M , date : 2017-08-16     
linux-oprofile-4.4.82-1.ph1.x86_64.rpm , sha256 : 14e5551e5fe9622ac6d60ec70971b1fb8cbc79752e3cc966d81e3ef50befc181 , size : 33K , date : 2017-08-16     
linux-sound-4.4.82-1.ph1.x86_64.rpm , sha256 : bcf0725eaa239de2fbacdbf3af6dfe27deab626828ee74e0990ee82bcd2609b4 , size : 227K , date : 2017-08-16     
linux-tools-4.4.82-1.ph1.x86_64.rpm , sha256 : 2b8e3b19f0c41e1a6877f9841c4a5580d1e706dadd440405618e80db9dca9cff , size : 729K , date : 2017-08-16     
libxml2-2.9.4-7.ph1.x86_64.rpm , sha256 : bee843ee2d4ceeef286148bea02958b845b52276a37d430d2075def018b4436e , size : 1.5M , date : 2017-08-16     
libxml2-debuginfo-2.9.4-7.ph1.x86_64.rpm , sha256 : 08dbfc9aa032143ab8e86284653a6da0064ac4b5cccf897be98bf918d3b5f953 , size : 2.9M , date : 2017-08-16       
libxml2-devel-2.9.4-7.ph1.x86_64.rpm , sha256 : 2c8420332a50ff56315561eef6fee8e6c2499e93649fbbcf33ee67f2fa6fa0b9 , size : 88K , date : 2017-08-16      
libxml2-python-2.9.4-7.ph1.x86_64.rpm , sha256 : d17ad2809f87b23071b452fc035dab6c5cb71b4c27fc522be236c13412cb88aa , size : 174K , date : 2017-08-16     
