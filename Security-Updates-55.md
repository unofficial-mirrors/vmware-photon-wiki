# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0025   
Type : Security   
Severity : [ Critical,Moderate ]   
Issued on : 2017-Jul-19   
Affected versions : PhotonOS 1.0   

## Details
An update of [linux,sqlite-autoconf,libxslt] packages for PhotonOS has been released.   

## Affected Packages:

### [Critical]
linux - [CVE-2017-11176](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-11176)  
sqlite-autoconf - [CVE-2017-10989](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10989)  

### [Moderate]
libxslt - [CVE-2015-9019](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-9019)  


## Solution
Install the update packages ( tdnf update package_name )   
Note: For package [ linux ] after updating, a reboot is required for taking effect.


## Updated Packages Information
linux-4.4.77-1.ph1.x86_64.rpm , sha256 : 26819ed44d309236dd238e3a629edf551a0038ed28e2853178b1799ed619ad89 , size : 18M , date : 2017-07-19    
linux-debuginfo-4.4.77-1.ph1.x86_64.rpm , sha256 : 404461bbda7e2bdcdc7f27b29a141210f4b494838cdfaca2c5ac92055190b624 , size : 345M , date : 2017-07-19   
linux-dev-4.4.77-1.ph1.x86_64.rpm , sha256 : 4ca2afcd0178dc74889f2aea679772c1544b3b7824d064ab441b0c35a01eed86 , size : 9.9M , date : 2017-07-19   
linux-docs-4.4.77-1.ph1.x86_64.rpm , sha256 : e86c9f0b947cfa7bcf2570afd8ca4cc2757b4aec3cba9ecd0a4b112d3f372b20 , size : 6.6M , date : 2017-07-19   
linux-drivers-gpu-4.4.77-1.ph1.x86_64.rpm , sha256 : f43a56dfe08cfd7282448cda0e38d0836b18f1a4e3828fe797c46b25ebaf4d64 , size : 1.4M , date : 2017-07-19   
linux-esx-4.4.77-1.ph1.x86_64.rpm , sha256 : 71878c63c1946975677fbe008a6eb49a4e2431ffa17493b577706bec680c25ae , size : 7.1M , date : 2017-07-19   
linux-esx-debuginfo-4.4.77-1.ph1.x86_64.rpm , sha256 : 8da183e50afa3ea13224f16104881e9d017917efe3e79d07f184fd54f38c679b , size : 155M , date : 2017-07-19   
linux-esx-devel-4.4.77-1.ph1.x86_64.rpm , sha256 : 43ee9fbf8b4544ca259273f0ae0dbfb8338ed87db758602b27bb90fb16914408 , size : 9.7M , date : 2017-07-19   
linux-esx-docs-4.4.77-1.ph1.x86_64.rpm , sha256 : bf06f9fd4740ab7a8cab1698c7c2390b1bea6fa6a1ff5afe913d721456df61b1 , size : 6.6M , date : 2017-07-19   
linux-oprofile-4.4.77-1.ph1.x86_64.rpm , sha256 : 10a9213e73037e52f3b05303c96f3a2d35945a559240138186f0b238e04e295a , size : 32K , date : 2017-07-19   
linux-sound-4.4.77-1.ph1.x86_64.rpm , sha256 : 6c9afe0b06b9e0b82e7a9133b6aab8703a5df96aef49a65b8c627f019c1968fb , size : 227K , date : 2017-07-19   
linux-tools-4.4.77-1.ph1.x86_64.rpm , sha256 : 3c54c472f4a671b357d37befc6b51e2007b9fb26a6dbcd0fb974d15b21043d29 , size : 728K , date : 2017-07-19   
sqlite-autoconf-3.18.0-2.ph1.x86_64.rpm , sha256 : e2611182e6c798c2c876e0eec89a4d76aa258e6a6b1ef2c6f6e2ef3d7c2f204d , size : 1.1M , date : 2017-07-19  
sqlite-autoconf-debuginfo-3.18.0-2.ph1.x86_64.rpm , sha256 : 3a77920a70f4344863e5573829f8b66ecd005b022ccac2a8b5543fee94834822 , size : 4.2M , date : 2017-07-19   
libxslt-1.1.29-4.ph1.x86_64.rpm , sha256 : 6f47d5994a245a74e303999ca2b58d9abc5667b17d1e00537d3d43075a49fd62 , size : 167K , date : 2017-07-19   
libxslt-debuginfo-1.1.29-4.ph1.x86_64.rpm , sha256 : e5a3edebad7bb7fb2185b39f1f9490769c5007d3258bfe2f13b15576bc5626a6 , size : 629K , date : 2017-07-19   
libxslt-devel-1.1.29-4.ph1.x86_64.rpm , sha256 : e0c2e6702fcea01c955cb9840c24715fd99c6332314cb02929475b651dfbc63b , size : 330K , date : 2017-07-19  
