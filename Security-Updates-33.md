# [Moderate] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0009  
Type : Security   
Severity : [Moderate]   
Issued on : 2017-Apr-10   
Affected versions : PhotonOS 1.0   

## Details
An update of [bash] packages for PhotonOS has been released.    

## Affected Packages:

### [Moderate]
bash - [CVE-2016-7543](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7543)   

## Solution
Install the update packages ( _tdnf update package_name_ )

## Updated Packages Information
bash-4.3.30-8.ph1.x86_64.rpm , sha256 : 38a2211a8ff84c5c49830c78bf30fe877290490a2207a786742fa742a9c1aa43 , size : 867K , date : 2017-04-10   
bash-debuginfo-4.3.30-8.ph1.x86_64.rpm , sha256 : 75197fd321392600d7e27763340d2b18826f31575e1da2e0a73a9d98f41ee1a3 , size : 1.4M , date : 2017-04-10   
bash-lang-4.3.30-8.ph1.x86_64.rpm , sha256 : a3ff15d56d3bd1c79b662730535d3905ea028f000fe1202dec3bbd1839633b91 , size : 1.2M , date : 2017-04-10    
