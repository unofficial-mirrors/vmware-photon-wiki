# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0014    
Type : Security   
Severity : [ Critical,Moderate ]   
Issued on : 2017-Apr-30   
Affected versions : PhotonOS 1.0   

## Details
An update of [linux] packages for PhotonOS has been released.   


## Affected Packages:

### [Critical]
linux - [CVE-2017-7618](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7618)  

[Moderate]  
linux - [CVE-2017-2671](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-2671)   

## Solution
Install the update packages ( _tdnf update package_name_ )     
Note: For package [ linux ] after updating, a reboot is required for taking effect.


## Updated Packages Information
linux-4.4.62-1.ph1.x86_64.rpm , sha256 : 9aae91c6b8f7d6428cb546c937517d2e83e4e6f870ac213206970a6c60b392fc , size : 17M , date : 2017-04-30   
linux-debuginfo-4.4.62-1.ph1.x86_64.rpm , sha256 : d23d4633bb2545344e057fcbb013ad56dda01e8e03fa7ca2685c8fbd78e18a07 , size : 336M , date : 2017-04-30  
linux-dev-4.4.62-1.ph1.x86_64.rpm , sha256 : 41ecce9577e58127d4a6cab4138937787582e2baa0c2486214afcdc853f15693 , size : 9.9M , date : 2017-04-30  
linux-docs-4.4.62-1.ph1.x86_64.rpm , sha256 : f03f080320fe2d60bd8991f84be96112bdfe64085d10076513c50f8790fa2ef7 , size : 6.6M , date : 2017-04-30   
linux-drivers-gpu-4.4.62-1.ph1.x86_64.rpm , sha256 : ad460473813d5b5adca3f981abc7c0492c59afae50f45d029e277bddcc181b56 , size : 1.4M , date : 2017-04-30   
linux-esx-4.4.62-1.ph1.x86_64.rpm , sha256 : 85688cab18823d5e3e16854aff5e05350e08bd3260f2c963efb10d499b96e59a , size : 7.0M , date : 2017-04-30   
linux-esx-debuginfo-4.4.62-1.ph1.x86_64.rpm , sha256 : 3cebf5434fec897a0e30416040604d468b3c9cd10f22b3c5c4f94e033e56e02e , size : 154M , date : 2017-04-30   
linux-esx-devel-4.4.62-1.ph1.x86_64.rpm , sha256 : 2a98a09022d72ea13ca8896afb0fa150f8847976c00c2b1a2fe80df2f12b1576 , size : 9.7M , date : 2017-04-30   
linux-esx-docs-4.4.62-1.ph1.x86_64.rpm , sha256 : 71009218aa698b59bf4ee131779341a8aec4d3d55be1a18685a47061e8978372 , size : 6.6M , date : 2017-04-30   
linux-oprofile-4.4.62-1.ph1.x86_64.rpm , sha256 : 2309581fe3866fcfe08ace1f0cfce0fd829c5903074a5cbd04f0b00b34e15f84 , size : 30K , date : 2017-04-30   
linux-sound-4.4.62-1.ph1.x86_64.rpm , sha256 : 07811d4bacba245d0e8e0d464c5eb9b5b7ccd42dc4af76ccc87d6dfeb4983a58 , size : 218K , date : 2017-04-30   
