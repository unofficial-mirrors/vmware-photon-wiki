# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2016-0015      
Type : Security       
Severity : [ Critical , Moderate ]      
Issued on : 2016-Dec-29        
Affected versions : PhotonOS 1.0     

## Details
An update of [openjdk,openjre,postgresql] packages for PhotonOS has been released.

## Affected Packages:

[Critical]      
openjdk - [CVE-2016-5582](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5582) [CVE-2016-5556](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5556)     
openjre - [CVE-2016-5582](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5582) [CVE-2016-5556](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5556)      

[Moderate]     
postgresql - [CVE-2016-5423](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5423)     
openjdk - [CVE-2016-5573](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5573)     


## Solution
Install the update packages ( _tdnf update package_name_ )    

## Updated Packages Information
openjdk-1.8.0.112-1.ph1.x86_64.rpm , sha256 : 9792ae23d804d1016ba9440a2b988c0ac7138af86d7402869174b0eb852075b4 , size : 12M , date : 2016-12-29     
openjdk-debuginfo-1.8.0.112-1.ph1.x86_64.rpm , sha256 : 41d62db20c94b5931d37b8ae0fdc5b3e5ad2476e3966d3568a4f465f91560d5a , size : 2.0M , date : 2016-12-29    
openjdk-doc-1.8.0.112-1.ph1.x86_64.rpm , sha256 : 66f3f523464687a67d6afecec576b0c065c8f1e221e55c6a860d287e0c9edc7c , size : 2.4M , date : 2016-12-29     
openjdk-sample-1.8.0.112-1.ph1.x86_64.rpm , sha256 : 0813ad974350a588c9471ada32d0aa443d4c78c054c0a1d7e383fd25aa5fb4ff , size : 448K , date : 2016-12-29     
openjdk-src-1.8.0.112-1.ph1.x86_64.rpm , sha256 : 52bd18ab5431e10c45bfd9220872d39f6b45c24339c63d40227e5ae6413df397 , size : 22M , date : 2016-12-29      
openjre-1.8.0.112-1.ph1.x86_64.rpm , sha256 : d816083cefea8beb391c8f9dadb128165b8e7c16b3d52ed1bec100791053f736 , size : 37M , date : 2016-12-29      
postgresql-9.5.3-4.ph1.x86_64.rpm , sha256 : ebf301c1488c2eaf9c5af8f05ae3e01415a9ccd8e83745afffd679b0b1d241fb , size : 6.2M , date : 2016-12-29      
postgresql-debuginfo-9.5.3-4.ph1.x86_64.rpm , sha256 : 2c55ffb638dcc45946d1c88d24c02f76a069f55901974d10abc53ff5cc1b64c6 , size : 509K , date : 2016-12-29      
postgresql-libs-9.5.3-4.ph1.x86_64.rpm , sha256 : 2084410bd1a999c5c8b77ec19375282a0af8ed82240afcb18aa8f68453b16722 , size : 1.3M , date : 2016-12-29       
