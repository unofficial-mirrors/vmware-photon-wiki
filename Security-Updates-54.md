# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0024  
Type : Security   
Severity : [ Critical/Moderate ]  
Issued on : 2017-Jul-13  
Affected versions : PhotonOS 1.0   

## Details
An update of [ncurses,openldap,libxml2,ruby] packages for PhotonOS has been released.  

## Affected Packages:
### [Critical]
ncurses - [CVE-2017-10684](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10684)  [CVE-2017-10685](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-10685)   

### [Moderate]
openldap - [CVE-2017-9287](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9287)   
libxml2 - [CVE-2017-9047](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9047) [CVE-2017-9048](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9048) [CVE-2017-9049](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9049) [CVE-2017-9050](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9050)  
ruby - [CVE-2017-6181](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6181)   

## Solution
Install the update packages ( tdnf update package_name )

## Updated Packages Information
ncurses-6.0-5.ph1.x86_64.rpm , sha256 : bb0e6912308c2355510c6aaade075077f2abd2b93e371dd35a1e8da4e2b79f4a , size : 1.9M , date : 2017-07-13   
ncurses-compat-6.0-5.ph1.x86_64.rpm , sha256 : 5aa1df7b519e42b02e1a38e960052d292f6db8f7ec3422ae8049090d27d94443 , size : 221K , date : 2017-07-13   
ncurses-debuginfo-6.0-5.ph1.x86_64.rpm , sha256 : c17c8bc38945101d55d4dcf5011f4db3e1146a8259afa4b102b559a59bba320f , size : 62K , date : 2017-07-13   
ncurses-devel-6.0-5.ph1.x86_64.rpm , sha256 : 769a9c00ee16fe47085e2ece2004f098e7b71cc4057d1b96857885d8b4afa886 , size : 393K , date : 2017-07-13  
openldap-2.4.43-3.ph1.x86_64.rpm , sha256 : 0942f364489c315061c8b0387786bd10e9f04a6654beca4dcbdc15c49b92a0e1 , size : 856K , date : 2017-07-13   
openldap-debuginfo-2.4.43-3.ph1.x86_64.rpm , sha256 : 8703ae8b4a1c49ba272ec10fb70893e86e8836f991848b775e799ee3449fc9eb , size : 1.1M , date : 2017-07-13  
libxml2-2.9.4-6.ph1.x86_64.rpm , sha256 : 67f648574e50a7b22e92bdc9796d81ea7dad78e7705c750fabfeca6931663f26 , size : 1.5M , date : 2017-07-13  
libxml2-debuginfo-2.9.4-6.ph1.x86_64.rpm , sha256 : 0e45bcd622c34db87b6b3e812a1a4efdc9aec1d98b77182f4e48b4af364f2068 , size : 2.9M , date : 2017-07-13   
libxml2-devel-2.9.4-6.ph1.x86_64.rpm , sha256 : e0f797f9415ec51ee461fe51e09d23b751e00a9ed8714daa433273122d14a9ad , size : 88K , date : 2017-07-13   
libxml2-python-2.9.4-6.ph1.x86_64.rpm , sha256 : 103b63f14a0a8e0b476d4ed5d04274a790e7fa0f88e75c1694dddaf4236351aa , size : 174K , date : 2017-07-13   
ruby-2.4.0-4.ph1.x86_64.rpm , sha256 : c817f4d8de2a48f4b580eeb8e2e996fcbe6159e07624f563b5278fed7f1e21ae , size : 12M , date : 2017-07-13   
ruby-debuginfo-2.4.0-4.ph1.x86_64.rpm , sha256 : 95d4281cac015f38b1a0154b67a2af020a945e50eff7256036b679fb295b253e , size : 16M , date : 2017-07-13   
