# [Critical] Photon OS Security Update

## Summary
Advisory ID: PHSA-2016-0014   
Type : Security    
Severity : [ Critical , Moderate ]   
Issued on : 2016-Dec-15   
Affected versions : PhotonOS 1.0   

## Details
An update of [ openssh , linux  ] packages for PhotonOS has been released.

## Affected Packages:   

[Critical]   
openssh - [CVE-2016-8858](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8858)    

[Moderate]    
linux - [CVE-2016-8655](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8655)    



## Solution
Install the update packages ( tdnf update _package_name_ )     
Note: For package [ linux ] after updating, a reboot is required for taking effect.    

## Updated Packages Information
openssh-7.1p2-5.ph1.x86_64.rpm , sha256 : c6971f4d74099e69c473153777d145e5f2370f570461596178bca78394193d43 , size : 1.8M , date : 2016-12-15   
openssh-debuginfo-7.1p2-5.ph1.x86_64.rpm , sha256 : 6bd6f7696942a774af6ee49049c5910f9abd60fdcff1e5967bda4ca2c7e316a2 , size : 2.9K , date : 2016-12-15   
linux-4.4.35-3.ph1.x86_64.rpm , sha256 : ba2a8aada424ecbf4ad44fce188f8089154692c5bdee9fcc45df7399cd99e2e1 , size : 17M , date : 2016-12-15  
linux-debuginfo-4.4.35-3.ph1.x86_64.rpm , sha256 : 55a1905a9ac470d59b958b1617f82fea3660e9f3e1291800312279eda4458e84 , size : 336M , date : 2016-12-15   
linux-dev-4.4.35-3.ph1.x86_64.rpm , sha256 : 9dfa723025dce425cee81e11d019a397ac123f6018e6f806e56ac9f84deb4d32 , size : 9.9M , date : 2016-12-15   
linux-docs-4.4.35-3.ph1.x86_64.rpm , sha256 : 64e02df0947a94cb9cc230c8276f9dbfd754a3e23a4fad0134b13784918c24e6 , size : 6.6M , date : 2016-12-15   
linux-drivers-gpu-4.4.35-3.ph1.x86_64.rpm , sha256 : 0558d82b6446bf06557e85e421e9f3949a6e8668a13c3b4e5b43484be2da94f6 , size : 1.4M , date : 2016-12-15   
linux-esx-4.4.35-4.ph1.x86_64.rpm , sha256 : 009482c046ae25e8f19f00bfa488a13a52e2b87a498341819a0bf3f963c3c4f2 , size : 6.9M , date : 2016-12-15    
linux-esx-debuginfo-4.4.35-4.ph1.x86_64.rpm , sha256 : 0f5caa2518d2380daf09747b3be922b03b19571741e0d718c7e1d8bb191c7075 , size : 4.1M , date : 2016-12-15    
linux-esx-devel-4.4.35-4.ph1.x86_64.rpm , sha256 : ab8722d254975b3aaa53a87cc6f0731996babf70dd87f1965f70c3cc4e4b9722 , size : 9.7M , date : 2016-12-15     
linux-esx-docs-4.4.35-4.ph1.x86_64.rpm , sha256 : cc6b8219e8ffe4d813ec390302f3d43a82f3cf184039449ae565e33ebb041fd6 , size : 6.6M , date : 2016-12-15    
linux-oprofile-4.4.35-3.ph1.x86_64.rpm , sha256 : 55787290034bdcb37bd052c00b4273063bca6c4bceef30ee3729877d7ef7e1bc , size : 30K , date : 2016-12-15    
linux-sound-4.4.35-3.ph1.x86_64.rpm , sha256 : cd00b1ee6bcc22d91c5d0ed449f020f17dbd5a16ed4960b1d000626b40ce5faa , size : 217K , date : 2016-12-15   
