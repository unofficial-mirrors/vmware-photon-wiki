# [Moderate] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0011   
Type : Security   
Severity : [ Moderate ]   
Issued on : 2017-Apr-14   
Affected versions : PhotonOS 1.0   

## Details
An update of [krb5,linux] packages for PhotonOS has been released.  

## Affected Packages:

### [Moderate]
krb5 - [CVE-2015-8631](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-8631)    
linux - [CVE-2017-7346](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7346) [CVE-2017-7308](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7308) [CVE-2017-7294](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7294) [CVE-2017-7187](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7187)  [CVE-2017-7184](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7184) [CVE-2017-7346](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7346)

## Solution
Install the update packages ( _tdnf update package_name_ )   
Note: For package [ linux ] after updating, a reboot is required for taking effect.

## Updated Packages Information
krb5-1.14-5.ph1.x86_64.rpm , sha256 : 51a43a8f4054b00d092e20bf4542a390a04df0d5286c834fc3988a0bbc36aa54 , size : 1.4M , date : 2017-04-13  
krb5-debuginfo-1.14-5.ph1.x86_64.rpm , sha256 : fb6e1d2e3eda599f6624f4ec9302c99007a009d91b998aab40a9ffb6f56a9e24 , size : 4.6M , date : 2017-04-13   
linux-api-headers-4.4.60-1.ph1.noarch.rpm , sha256 : 43176329377aca7df3bac7d496c8c46f16a0a3a6e03312de1fdb18418c880608 , size : 1.1M , date : 2017-04-13   
linux-4.4.60-1.ph1.x86_64.rpm , sha256 : ece42414e8b040d677f748574e28b7d4945872ff073c9c522c71ed828a281259 , size : 17M , date : 2017-04-13  
linux-debuginfo-4.4.60-1.ph1.x86_64.rpm , sha256 : 8477e3f523e72a3291ce164116407127a63bd1d8d0ecd234db32ec515812b706 , size : 336M , date : 2017-04-13   
linux-dev-4.4.60-1.ph1.x86_64.rpm , sha256 : 8e2a527070f3e18dc3b686df35a83ca3a49399c3e7918178f1c1ea7cdd19c44f , size : 9.9M , date : 2017-04-13   
linux-docs-4.4.60-1.ph1.x86_64.rpm , sha256 : 0dd6f6a94c5e2dfbff20f693d54f6e8906e04bb6ef1733bcac006337d33fa113 , size : 6.6M , date : 2017-04-13   
linux-drivers-gpu-4.4.60-1.ph1.x86_64.rpm , sha256 : 3acd8bd270cdb53ff7654c4fd19fd127742f61fa2af2cd3ff1241634f7988bae , size : 1.4M , date : 2017-04-13   
linux-esx-4.4.60-1.ph1.x86_64.rpm , sha256 : 733ac025a7659f95b179aa63aa36e6e8a8a4439dc3b7ff54f2d5a3b61bf78356 , size : 7.0M , date : 2017-04-13   
linux-esx-debuginfo-4.4.60-1.ph1.x86_64.rpm , sha256 : d07793fe57d0c27c263e940a4354d5cddf5087fedad2eef3a078dd23b30281fc , size : 4.1M , date : 2017-04-13   
linux-esx-devel-4.4.60-1.ph1.x86_64.rpm , sha256 : 5b7a303d826b034ca259e7b430c7e8027a46ae30eb6fc004edff54a0e91d5291 , size : 9.7M , date : 2017-04-13   
linux-esx-docs-4.4.60-1.ph1.x86_64.rpm , sha256 : c0699a0bd21ec103040b1ad759f0bc2b17f75b70ac2825b98b342d5c6d5c0327 , size : 6.6M , date : 2017-04-13   
linux-oprofile-4.4.60-1.ph1.x86_64.rpm , sha256 : fdb0188d6b3c5ae74a41cfad4ba35a02adb3472c6cf0d2e467a11d9b6374844e , size : 30K , date : 2017-04-13   
linux-sound-4.4.60-1.ph1.x86_64.rpm , sha256 : 8f06a4ad21443db9fa6d62e77ba84dc665a0472b2cfc1f704edc6c0d7e0367a6 , size : 218K , date : 2017-04-13   
linux-tools-4.4.60-1.ph1.x86_64.rpm , sha256 : 9b77d090f1100735e7ec0f134cc0c1a6642f6ef2b61fee059b5076d18bcb4654 , size : 704K , date : 2017-04-13   
linux-tools-debuginfo-4.4.60-1.ph1.x86_64.rpm , sha256 : 540fb881e2bbe01979f668df2789d73be439aab0f1ba40d6654e9c36ac7b9065 , size : 3.4M , date : 2017-04-13       