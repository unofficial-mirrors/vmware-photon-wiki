# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0002    
Type : Security     
Severity : [ Critical ]    
Issued on : 2017-Jan-23     
Affected versions : PhotonOS 1.0    

## Details
An update of [ruby] packages for PhotonOS has been released.   

Affected Packages:      

### [Critical]    
ruby - [CVE-2016-2339](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-2339)   

## Solution
Install the update packages ( _tdnf update package_name_ )


## Updated Packages Information     
ruby-2.4.0-1.ph1.x86_64.rpm , sha256 : 4f7f1d3c5f49ac412efefb542ebc1bc6fb7018de2b04db1a2affba18057524ac , size : 12M , date : 2017-01-23    
ruby-debuginfo-2.4.0-1.ph1.x86_64.rpm , sha256 : 7b6f43ed0cf00a450bbcccdfe085313701b46da335a0e1e4507bb48370a1ea73 , size : 16M , date : 2017-01-23    
