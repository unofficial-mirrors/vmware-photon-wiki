# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0007   
Type : Security   
Severity : [ Critical ]   
Issued on : 2017-Mar-8   
Affected versions : PhotonOS 1.0   

## Details
An update of [vim] packages for PhotonOS has been released.    

## Affected Packages:    

### [Critical]
vim - [CVE-2017-6349](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6349) [CVE-2017-6350](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6350)

## Solution     
Install the updated packages ( _tdnf update package_name_ )     
## Updated Packages Information
vim-7.4-8.ph1.x86_64.rpm , sha256 : 59ad2ae0ba9d3095b3d2877a7ad965e235c4c855a8e80fa74776f7d267498673 , size : 1022K , date : 2017-03-08     
vim-extra-7.4-8.ph1.x86_64.rpm , sha256 : fd42acc6f35d63776214e081708ccd0071b7421afbc99274f3c83e0d5ea96284 , size : 8.3M , date : 2017-03-08    
