# [Moderate] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0032  
Type : Security  
Severity : [Moderate]   
Issued on : 2017-Sep-6  
Affected versions : PhotonOS 1.0   

## Details
An update of [linux] packages for PhotonOS has been released.   

## Affected Packages:
### [Moderate]
linux - [CVE-2017-11600](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-11600)

## Solution
Install the update packages ( tdnf update package_name )   
Note: For package [ linux ] after updating, a reboot is required for taking effect.   
## Updated Packages Information
linux-api-headers-4.4.86-1.ph1.noarch.rpm , sha256 : 2c14657e9fa28ab8fcfa7329a621c7616302e4293044bdd492c79f3076c046b3 , size : 1.1M , date : 2017-09-06   
linux-4.4.86-1.ph1.x86_64.rpm , sha256 : 2bdcb3e6e3d4b74d024127d939b99cb458e4ac25f44405cd7ea641885ebc4210 , size : 18M , date : 2017-09-06    
linux-debuginfo-4.4.86-1.ph1.x86_64.rpm , sha256 : da9936ff215cabc5b57ddb7b2961fdcd5cb8d0a76328b35b2fc4950d0d2a5c0b , size : 346M , date : 2017-09-06   
linux-dev-4.4.86-1.ph1.x86_64.rpm , sha256 : c3584e2c2808cfc7d07ed46d7f552a4dd4952e8e7c95c21f39a1a012a20eceaa , size : 10M , date : 2017-09-06   
linux-docs-4.4.86-1.ph1.x86_64.rpm , sha256 : 97e00ca3754a6b771dab207d8d4e93b51155684ab826ebdad4e09884af3fdd58 , size : 6.6M , date : 2017-09-06   
linux-drivers-gpu-4.4.86-1.ph1.x86_64.rpm , sha256 : bd31e7d97cba44b3d33b8a470f1a82436b2afba0aa38ce03218561ba9d486e2c , size : 1.4M , date : 2017-09-06   
linux-esx-4.4.86-1.ph1.x86_64.rpm , sha256 : bd5de67087406fbc8959a3c4fea987c2091944cc2607a22a9f5bc5af8c564a50 , size : 7.1M , date : 2017-09-06  
linux-esx-debuginfo-4.4.86-1.ph1.x86_64.rpm , sha256 : 269c776b46014015b9c846d5e4f41f9528ac22b50e71bc2f171601f3f7bb443b , size : 156M , date : 2017-09-06   
linux-esx-devel-4.4.86-1.ph1.x86_64.rpm , sha256 : acd74109bd2b604c42be2a7937393b1e6e51ccf66e07f43dd887c8dab046fe2b , size : 9.7M , date : 2017-09-06   
linux-esx-docs-4.4.86-1.ph1.x86_64.rpm , sha256 : d2ce0520a9cc3f189054493dad7c5c086263b015fc4b2b1395541d3b3516279d , size : 6.6M , date : 2017-09-06   
linux-oprofile-4.4.86-1.ph1.x86_64.rpm , sha256 : 1b4f9b6d42158b1ad716184253bb0596c6bd4f67eca19068c447338e9c057c0e , size : 33K , date : 2017-09-06   
linux-sound-4.4.86-1.ph1.x86_64.rpm , sha256 : 42e094f51b89d23165d6ea872ec0db6520654407180b0b45c23d5104bbb0cdca , size : 227K , date : 2017-09-06     
linux-tools-4.4.86-1.ph1.x86_64.rpm , sha256 : a4bd3c31c64c85035f951a95189c12d528c8f0e4751fcbe99f4566338b4ac2a2 , size : 730K , date : 2017-09-06     
