# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0001    
Type : Security    
Severity : [ Critical ]    
Issued on : 2017-Jan-11    
Affected versions : PhotonOS 1.0   

## Details
An update of [openssh,linux,libxml2] packages for PhotonOS has been released.

## Affected Packages:

### [Critical]
openssh - [CVE-2016-10009](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-10009)

### [Moderate]
openssh - [CVE-2016-10012](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-10012) [CVE-2016-10010](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-10010)    
linux - [CVE-2016-10088](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-10088) [CVE-2016-9793](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-9793) [CVE-2016-9754](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-9754)      
libxml2 - [CVE-2016-9318](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-9318)     

## Solution
Install the update packages ( _tdnf update package_name_ )    
Note: For package [ linux ] after updating, a reboot is required for taking effect.    

## Updated Packages Information
openssh-7.4p1-1.ph1.x86_64.rpm , sha256 : 11c4e3c581e0e7deb43c513caa0a8bf0e3d56521b24327b77f4424c4260dd5d9 , size : 1.9M , date : 2017-01-11     
openssh-debuginfo-7.4p1-1.ph1.x86_64.rpm , sha256 : 02c79483cee13219588206420b367ab84bd79f661419707c14c573d006fc2806 , size : 3.0K , date : 2017-01-11    
linux-api-headers-4.4.41-1.ph1.noarch.rpm , sha256 : 701766dec6b2716869ab57a6d72fc98ce06d6aeb63520b21a7386536f0c22253 , size : 1.1M , date : 2017-01-11    
linux-4.4.41-1.ph1.x86_64.rpm , sha256 : 56215d7555dacd4d2dab889a407d97083b12e3514873ff2f23f5639bfc59aace , size : 17M , date : 2017-01-11     
linux-debuginfo-4.4.41-1.ph1.x86_64.rpm , sha256 : 7f87bb4a028b9cb92e9a5d065eb162c68edac70a172b25e32fd8353784f5fa20 , size : 336M , date : 2017-01-11     
linux-dev-4.4.41-1.ph1.x86_64.rpm , sha256 : ec3c7dd490d71fcb3ab497a6d815d3f1e1709e0bbef570eaf56028f10afa6530 , size : 9.9M , date : 2017-01-11     
linux-docs-4.4.41-1.ph1.x86_64.rpm , sha256 : 306b2f1b293e69c3c35d80953bf9eccd7d1fbdf815cf4c7a77b62489b737d0f4 , size : 6.6M , date : 2017-01-11     
linux-drivers-gpu-4.4.41-1.ph1.x86_64.rpm , sha256 : 193e4d2ab28df622c00d36893745fe95cccc86add0596df8c5a955c82cb64947 , size : 1.4M , date : 2017-01-11   
linux-esx-4.4.41-1.ph1.x86_64.rpm , sha256 : 90b730d95a668bff454d9d0529eb31540e260034708334fcae51d0082d27f74d , size : 6.9M , date : 2017-01-11    
linux-esx-debuginfo-4.4.41-1.ph1.x86_64.rpm , sha256 : 5fccbd07f43e2e8668c3d14aaf022a56ac0e083665264b0119f6369050403459 , size : 4.1M , date : 2017-01-11   
linux-esx-devel-4.4.41-1.ph1.x86_64.rpm , sha256 : 00ec39529110a182c151cbea8a1fe8edda6c5aa1c4136b04da77eafa3c0f6e60 , size : 9.7M , date : 2017-01-11   
linux-esx-docs-4.4.41-1.ph1.x86_64.rpm , sha256 : 21c9b566d12ff226d1c6843040b4673611aa79502773292674c64bf6234d48d3 , size : 6.6M , date : 2017-01-11    
linux-oprofile-4.4.41-1.ph1.x86_64.rpm , sha256 : 36092f89e0d2791f910b49af5546d09f0481a75a0bf119ed65486a4d9a799bf9 , size : 30K , date : 2017-01-11   
linux-sound-4.4.41-1.ph1.x86_64.rpm , sha256 : 07b131823b646d5d4aa8db053041e130f317f4817580fef04c0ab9e8501939db , size : 217K , date : 2017-01-11    
linux-tools-4.4.41-1.ph1.x86_64.rpm , sha256 : 96cf62891b44d3ee3d1bf036bb72f13cfd26be0e978245fc2ea8e8fdbfaeee44 , size : 704K , date : 2017-01-11     
linux-tools-debuginfo-4.4.41-1.ph1.x86_64.rpm , sha256 : bd725fd5137f233d9dc88d48edb06f92c6dc573805fe65e06dc0e01a7513e59e , size : 3.4M , date : 2017-01-11    
libxml2-2.9.4-3.ph1.x86_64.rpm , sha256 : 760b6a781969ae49c374aa8659b6bfd809d533101e66ed8abaf5d2bc8e91aff2 , size : 1.5M , date : 2017-01-11    
libxml2-debuginfo-2.9.4-3.ph1.x86_64.rpm , sha256 : c7a980330968f158cf6575714e9cdca72139f116ebb68e97d9cb34e7dcc14bf8 , size : 2.7M , date : 2017-01-11   
libxml2-devel-2.9.4-3.ph1.x86_64.rpm , sha256 : 1c8f22479b329b79d21a5b420dbe521bd3b7ff3954190e6f80d2782e4833cb94 , size : 88K , date : 2017-01-11    
libxml2-python-2.9.4-3.ph1.x86_64.rpm , sha256 : 224b96592027f3d7a31fa493696f18da40ae5386b8c1b58cb84fd876f95fc773 , size : 174K , date : 2017-01-11   

