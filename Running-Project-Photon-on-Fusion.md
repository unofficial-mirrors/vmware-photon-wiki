# What is Photon OS?
Photon OS is a tech preview of an open source, Linux container host runtime optimized for vSphere. Photon OS is extensible, lightweight, and supports the most common container formats including Docker, Rocket (rkt) and Garden.
Photon OS includes a small footprint, yum-compatible, package-based lifecycle management system – called “tdnf”- and, alternatively, supports an rpm-ostree image-based system versioning.
When used with development tools and environments such as VMware Fusion, VMware Workstation, HashiCorp (Vagrant and Atlas) and production runtime environment (vSphere, vCloud Air), Photon OS allows seamless migration of container based apps from development to production.  

# Introduction
This document explains how to get started using Photon OS as a runtime environment for Linux containers by running Photon OS as a virtual machine within VMware Fusion or VMware Workstation.  This guide will provide step-by-step instructions on downloading Photon OS, provide details of the various install options and provide a walkthrough of installing the full Photon OS distribution. 

Once Photon OS is installed, this guide will also provide instructions on how to demonstrate how simple it can be to deploy a containerized application with Docker and will highlight the installation of a web server simply by running one command!

# Photon OS Install - Prerequisites
In order to install and start using Photon OS with VMware Fusion the following pre-requisites must be satisfied:
* VMware Fusion or VMware Workstation must be installed
* Recommended 2GB of free RAM
* Recommended at least 300MB of free disk space
* Photon OS ISO or OVA

This guide is based on the version 8.1.1 of VMware Fusion Professional as per the Screenshot below.  Our recommendation is to always use the latest version; although, anything from v7.0 onwards should work as advertised.  Should you encounter any issues, please let us know on Twitter @VMwarePhoton or on our [GitHub project page](https://github.com/vmware/photon).

![](https://cloud.githubusercontent.com/assets/11306358/16094880/c7da2cbe-3307-11e6-9fe9-936cd64a4817.png)

# Before You Begin
This document covers both [installing the Photon OS from an ISO](#Installing-Photon-OS-on-VMware-Fusion-from-an-ISO-Image) and creating a Photon OS environment by [importing the Photon OS OVA](#Importing-the-Photon-OS-OVA) on VMware Fusion. The steps shown should be nearly identical for VMware Workstation users, as well. 

The first step is decide whether to use the ISO or the OVA. Because of the nature of an OVA, if you go that route, you're getting a pre-installed version of Photon OS. The OVA benefits from a simple import process and some kernel tuning for VMware environments; however, since it's a pre-installed version, the set of packages that are installed are predetermined. Any additional packages that you need can be installed using tdnf. The ISO, on the other hand, allows for a more complete installation or automated installation via kickstart. 

If you're just looking for the fastest way to get up and running, start with the OVA. 

Once you've decided which way to install, you'll need to download the correct binaries.

## Downloading Photon OS

First, [download Photon OS](https://github.com/vmware/photon/wiki/Downloading-Photon-OS).

# Importing the Photon OS OVA 

Using the OVA is the easiest way to create a Photon OS VM. Once you’ve downloaded the OVA, open VMware Fusion and select, “Import …” from the File menu. This will open the “Choose an Existing Virtual Machine” wizard. Use the “Choose File …” button to locate and select the Photon OS OVA.
Note: The “Import” operation is specific to Fusion. For Workstation users, simply double-clicking on the OVA will start the import. 

![](https://cloud.githubusercontent.com/assets/11306358/16094763/4ac80e12-3307-11e6-8e7b-10ea353ddbfc.png)

Click “Continue” and provide the name and storage location for your Photon OS VM. Then, click "Save."

![](https://cloud.githubusercontent.com/assets/11306358/16094759/4ac50000-3307-11e6-8a34-538f36b95f64.png)

Clicking “Save” will immediately present the Photon OS EULA. In order to start the import process, you'll need to click "Accept" to accept the EULA. 

![](https://cloud.githubusercontent.com/assets/11306358/16094758/4ac50668-3307-11e6-86ce-943afa12946f.png)

Once the import is complete, you should get confirmation that the import was Finished and a summary of the settings for your Photon OS VM. Click “Customize Settings” to change the operating system, as recognized by the hypervisor. Within the "General" System Settings for the newly imported VM, click the selection box next to "OS" and select, "VMware Photon 64-bit," as shown below.

![](https://cloud.githubusercontent.com/assets/11306358/16095187/24abc4f6-3309-11e6-9faa-c4e7b15ba63a.png) 

Close the settings window and your Photon OS VM is ready to power on.

![](https://cloud.githubusercontent.com/assets/11306358/16094764/4acf69dc-3307-11e6-9d62-7dd37546a233.png)

Clicking “Finish” will immediately power on your Photon OS VM. Once the VM is booted, you will be presented with a login prompt. Because of limitations within OVA support on Fusion and Workstation, it was necessary to specify a default password for the OVA option. However, all Photon OS instances that are created by importing the OVA will require an immediate password change upon login. The default account credentials are:

`Username: root`
`Password: changeme`

As soon as you enter these credentials, you will be forced to create a new password by entering the new password twice before you can access the shell prompt.

![](https://cloud.githubusercontent.com/assets/11306358/16094765/4ad06ce2-3307-11e6-827f-e61107185f42.png)
 
# Installing Photon OS on VMware Fusion from an ISO Image	
With the latest Photon OS ISO image downloaded into a folder of your choice, Open VMware Fusion and Select “File->New.” The following screen will appear: 

![](https://cloud.githubusercontent.com/assets/11306358/14651747/df593c52-0636-11e6-9cbe-bfd0db9bfa89.png)

Select "Create a custom virtual machine" from the "Select the Installation Method" dialog, then, click continue. On the "Choose Operating System" dialog, select, "Linux" in the left-hand column and "VMware Photon 64-bit" in the right-hand column. 

![fusion7](https://cloud.githubusercontent.com/assets/11306358/14651749/df6fbd74-0636-11e6-8070-a584bddf39f5.png)

Unless you're installing into an existing machine, select, "Create a new virtual disk" from the "Choose a Virtual Disk dialog, then click "Continue."

![fusion7b](https://cloud.githubusercontent.com/assets/11306358/14651751/df7428a0-0636-11e6-85b2-64e2dff9be84.png)

You're almost finished; but, before finishing the Photon OS Virtual Machine Creation, we strongly recommend that you customize the virtual machine and remove any unwanted devices that are not needed for a container runtime environment.  

![fusion9](https://cloud.githubusercontent.com/assets/11306358/14651750/df7385f8-0636-11e6-9310-0d7f375dd7b5.png) 

To remove unnecessary devices, from the screen shown below, select “Customize Settings."

First, choose a name for your Virtual Machine and the folder into which you would like to create the Virtual Machine.  If the default folder of “Virtual Machines” is acceptable, click “Save”.  

![](https://cloud.githubusercontent.com/assets/11306358/16105478/724ea650-3350-11e6-8e69-272f6a1f6097.png)

The virtual machine will be created and a new screen will appear, as shown below, that will allow virtual hardware customization to the new virtual machine. If it does not automatically appear, open "Settings" from the Virtual Machine menu bar. 

![fusion11](https://cloud.githubusercontent.com/assets/11306358/14653053/cd434ffc-063c-11e6-9861-924489b26d75.png)

It is our recommendation that the following components are removed, since they’re not used by Photon OS:
* Select “Sound Card” and un-tick the “Connect Sound Card” Option and click "Remove Sound Card." Confirm your action and return to the VM Settings by clicking “Show All.”
* Select “Camera” and press the “Remove Camera” button in the bottom left hand corner, confirm your action and then select “Show All” to return to the VM Settings.
* Select “Printer” and press the “Remove Printer Port” button in the bottom left hand corner, confirm your action and then select “Show All” to return to the VM Settings.
* Select “USB & Bluetooth” and uncheck the “Share Bluetooth devices with Linux” setting and then select “Show All”
* Select “Display” and ensure that the “Accelerate 3D Graphics” option is unchecked (it should be unchecked, by default) and, then, return to the VM Settings by selecting “Show All.”
* Select “Advanced” and ensure that the “Pass Power Status to VM” option is unchecked. Select “Show All," but do not close the VM Settings window.

At this stage we have now made all the necessary customizations and we are ready to select the Photon OS ISO image to boot and begin the installation process. 

From the Virtual Machine Settings menu in Fusion, select "CD/DVD (IDE)" and, as shown below, point to the downloaded Photon OS ISO and ensure that there is a check in the "Connect CD/DVD Drive" box.

![](https://cloud.githubusercontent.com/assets/11306358/16105475/72391e0c-3350-11e6-94a2-64587a06e838.png)

Return to the Fusion main menu, select the Photon OS Virtual Machine and press the “Play” button to power on the host and start the installation.
Within a few seconds the Photon OS Installer Boot Menu will appear.

![](https://cloud.githubusercontent.com/assets/11306358/16105477/724e902a-3350-11e6-8482-7544d2a6c3fb.png)

Select – “Install” to proceed. 

![](https://cloud.githubusercontent.com/assets/11306358/16105480/7251abca-3350-11e6-8f8e-d26244fd75ff.png)

After you accept the EULA, the Installer will detect one disk, which should be the 8GB volume configured as part of the virtual machine creation. Select the disk and press enter.  You will be prompted to confirm it is okay to erase the entire disk, select “Yes” to accept and proceed with the installation.
You will now be presented with four installation options:

![fusion15](https://cloud.githubusercontent.com/assets/11306358/14651982/d464f31c-0637-11e6-938d-5d6132ccd63f.png)

Each install option provides a different runtime environment, depending on your requirements:

* Photon Minimal: Photon Minimum is a very lightweight version of the container host runtime that is best suited for container management and hosting. There is sufficient packaging and functionality to allow most common operations around modifying existing containers, as well as being a highly performant and full-featured runtime. 

* Photon Full: Photon Full includes several additional packages to enhance the authoring and packaging of containerized applications and/or system customization. For simply running containers, Photon Full will be overkill. Use Photon Full for developing and packaging the application that will be run as a container, as well as authoring the container, itself. For testing and validation purposes, Photon Full will include all components necessary to run containers. 

* Photon OSTree Host: This installation profile creates a Photon OS instance that will source its packages from a central rpm-ostree server and continue to have the library and state of packages managed by the definition that is maintained on the central rpm-ostree server. Use Photon OStree Hosts when you are interested in experimenting with the concept of a centrally authored and maintained OS version. This concept of treating the OS as a versioned, atomic entity can simplify lifecycle and security management at scale. 

* Photon OSTree Server: This installation profile will create the server instance that will host the filesystem tree and managed definitions for rpm-ostree managed hosts created with the “Photon OSTree Host” installation profile. Most environments should need only one Photon OSTree Server instance to manage the state of the Photon OSTree Hosts. Use Photon OSTree Server when you are establishing a new repository and management node for Photon OS hosts.  

For the purposes of this how-to guide, select option 1, “Photon Minimal.”  Once this selection is highlighted, press the Enter key on your keyboard.

You will now be prompted for a hostname. Photon OS will prepopulate a randomly generated, unique hostname; you can either use this suggestion or enter your own hostname. After selecting a hostname and pressing “Enter,” you will be prompted to first enter and, then, confirm the system root password.

*Note: Photon OS will not permit commonly used dictionary words to be set as a root password.*

After confirming the password, the installation process should begin.
Installation times will vary based on system hardware and installation options, but most installations complete in less than one minute. Once the install is complete you will get a confirmation prompt on the screen stating “Congratulations, Photon has been installed in xx secs, Press any key to continue to boot…”  - Press any key and the virtual machine will reboot into Photon OS.

![](https://cloud.githubusercontent.com/assets/11306358/16105476/724e70c2-3350-11e6-84af-0c487266108d.png)

As the initial boot process begins, you will see the Photon splash screen before you are taken to a login prompt.

![](https://cloud.githubusercontent.com/assets/11306358/16094764/4acf69dc-3307-11e6-9d62-7dd37546a233.png)

At the login prompt, enter “root” as the username and provide the password chosen during the installation. 

You have now successfully setup Photon OS and are ready to use your container runtime environment.

# Installing a Containerized Application to Help Demonstrate Capability
Now that you have your container runtime environment up and running, you may be wondering, “what can I do now?” A command prompt is not the most exciting!  To help to demonstrate the ease in which you can deploy a containerized application, we will showcase how you can quickly get a Web Server up and running.
For this example, we will use the popular open source Web Server Nginx. The Nginx application has a customized VMware package and published as a dockerfile and can be downloaded, directly, through the Docker module from the Docker Hub.

To run Docker from the command prompt, enter the command below to initialize the docker engine:

`systemctl start docker`

To ensure Docker daemon service runs on every subsequent VM reboot, enter:

`systemctl enable docker`

Now the Docker daemon service is running, it is a simple task to “pull” and start the Nginx Web Server container from Docker Hub.  To do this, type the following command:

`docker run -d -p 80:80 vmwarecna/nginx`

This will then pull the Nginx Web Server files and appropriate dependent container filesystem layers to ensure this containerized application can run.  You will see a screenshot similar to below, as the container and dependencies are downloaded and the container is prepared to run:

![fusion19](https://cloud.githubusercontent.com/assets/11306358/9568066/b3950dd8-4f04-11e5-9333-ac0551a22ace.jpg)

Once “docker run” process is completed, you will be returned to the command prompt.  You now have a fully active website up and running in a container!

To test that your Web Server is active, we need to get the IP address of the Photon OS Virtual Machine. To get the IP address, enter the following command ifconfig. This will now display a list of adapters connected to the virtual machine.  Typically, the web server daemon will be bound on “eth0.”  
 
Start a browser on your host machine and enter the IP address of your Photon OS Virtual Machine.  The following screen will appear and that will show that your web server is active: -

![fusion20](https://cloud.githubusercontent.com/assets/11306358/9568067/b3b6e278-4f04-11e5-93f6-de8383530518.jpg)

You can now run any other containerized application from Docker Hub or your own containerized application within Photon OS.

**We hope you enjoy using Photon OS as much as we enjoy creating it!**