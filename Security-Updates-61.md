# [Important] Photon OS Security Update

## Summary
Advisory ID: PHSA-2017-0028   
Type : Security   
Severity : [Important]   
Issued on : 2017-Aug-10   
Affected versions : PhotonOS 1.0  

## Details
An update of [linux] packages for PhotonOS has been released.   

## Affected Packages:
### [Important]
linux - [CVE-2017-7541](https://nvd.nist.gov/vuln/detail?vulnId=CVE-2017-7541) [CVE-2017-11473](https://nvd.nist.gov/vuln/detail?vulnId=CVE-2017-11473)

## Solution
Install the update packages ( tdnf update package_name )    
Note: For package [ linux ] after updating, a reboot is required for taking effect.   
## Updated Packages Information
linux-4.4.79-1.ph1.x86_64.rpm , sha256 : bd2ca34d771e5cdfcf4e79f45ddbd535113f4a3efaf4ba02356cbef5c8cbd506 , size : 18M , date : 2017-08-10   
linux-debuginfo-4.4.79-1.ph1.x86_64.rpm , sha256 : 94c043ffcaf2013bbb10102632985b2eb9bd0c08f9c3168052a012db2a1bbff6 , size : 345M , date : 2017-08-10   
linux-dev-4.4.79-1.ph1.x86_64.rpm , sha256 : 7bd5e694a522b2457058ce54c21ef9dd235160790dd8e8cdef7aac35b1f32875 , size : 9.9M , date : 2017-08-10  
linux-docs-4.4.79-1.ph1.x86_64.rpm , sha256 : 1f8704cf61a7626093b3b60a7f42f79070ccc25a99ce14991914ffd53891e049 , size : 6.6M , date : 2017-08-10   
linux-drivers-gpu-4.4.79-1.ph1.x86_64.rpm , sha256 : bfb2c1e020c8cb904dd971d11edee4d6416df0326b30e94c62cb79a4d536a9e2 , size : 1.4M , date : 2017-08-10   
linux-esx-4.4.79-1.ph1.x86_64.rpm , sha256 : 03c33ba741ed6e2511d673881b6079d5605cb1772dc046bbd7987a93a3c033c9 , size : 7.1M , date : 2017-08-10   
linux-esx-debuginfo-4.4.79-1.ph1.x86_64.rpm , sha256 : d1f84dd8dbf90ceacec7f83a594e94f3d948298f9152d90c6756e859eea5613f , size : 155M , date : 2017-08-10   
linux-esx-devel-4.4.79-1.ph1.x86_64.rpm , sha256 : cee6bd010ea907499b99867e272ff1cf8a235f47931b8df108cb863215f743b5 , size : 9.7M , date : 2017-08-10     
linux-esx-docs-4.4.79-1.ph1.x86_64.rpm , sha256 : 86b937844df72c882f859c2dc2b270e1bc13dfbe83b516a2b0abd69bfc244488 , size : 6.6M , date : 2017-08-10     
linux-oprofile-4.4.79-1.ph1.x86_64.rpm , sha256 : 48d7d6ee4170408ab3684259c9f65014a959b42d1303aac7495a7be476f57d31 , size : 33K , date : 2017-08-10        
linux-sound-4.4.79-1.ph1.x86_64.rpm , sha256 : 4efc133ff59f39764d9e108c06f0625ee6c10be2c5aacb24bc0bac26841d4955 , size : 227K , date : 2017-08-10         
linux-tools-4.4.79-1.ph1.x86_64.rpm , sha256 : 6b7f06bbf8b3bb4d022b295530f8f536d32ef1bfcfdab449a71fa8f9a12c2b52 , size : 729K , date : 2017-08-10    
