# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0020  
Type : Security  
Severity : [Critical]  
Issued on : 2017-Jun-15   
Affected versions : PhotonOS 1.0   

## Details
An update of [openvswitch] packages for PhotonOS has been released.

## Affected Packages:

### [Critical]
openvswitch - [CVE-2017-9264](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9264)

## Solution
Install the update packages ( tdnf update package_name )

## Updated Packages Information
openvswitch-2.6.1-3.ph1.x86_64.rpm , sha256 : 32ee89dfb30dcf93cd1689ed40f576eb1baf3f9ef33eb1d4ee05023b1b00c3b5 , size : 505K , date : 2017-06-15  
openvswitch-debuginfo-2.6.1-3.ph1.x86_64.rpm , sha256 : 8987a1af4ed8b6aea53d8ebd6608a0361cd7c1acc538a8a8b9b09648989c0bc3 , size : 6.7M , date : 2017-06-15  
openvswitch-devel-2.6.1-3.ph1.x86_64.rpm , sha256 : 0e0a8695c8943bd75e57b296c3295a199dbca69044a6ac8f924b4ff4021fb765 , size : 3.1M , date : 2017-06-15  
openvswitch-doc-2.6.1-3.ph1.x86_64.rpm , sha256 : c369e703cc8e89b396e8a7ef1a7b17dd1f823d1c63bd97ae58587210cc6aa4dc , size : 297K , date : 2017-06-15  
