# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0021  
Type : Security  
Severity : [Critical,Moderate,Low]  
Issued on : 2017-Jun-22   
Affected versions : PhotonOS 1.0   

## Details
An update of [zlib,bindutils,ruby,krb5,sudo] packages for PhotonOS has been released.

## Affected Packages:

### [Critical]
zlib - [CVE-2016-9841](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-9841) [CVE-2016-9843](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-9843)   
bindutils - [CVE-2016-2776](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-2776)   
ruby - [CVE-2017-9224](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9224) [CVE-2017-9225](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9225) [CVE-2017-9227](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9227) [CVE-2017-9229](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9229)
### [Moderate]
sudo - [CVE-2017-1000367](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-1000367) [CVE-2016-1000368](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-1000368)
### [Low]
krb5 - [CVE-2016-3120](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-3120)

## Solution
Install the update packages ( tdnf update package_name )


## Updated Packages Information
zlib-1.2.8-5.ph1.x86_64.rpm , sha256 : c0f1f6782688255c06bceac01d1a56aa51c92a5d85dffa29bd7d2614bf75aeb3 , size : 60K , date : 2017-06-22  
zlib-debuginfo-1.2.8-5.ph1.x86_64.rpm , sha256 : 36e02a2fee3ef78fc6f7c819e188f480026c67b152ea7aa01fc27fd73ed845ee , size : 6.7K , date : 2017-06-22  
zlib-devel-1.2.8-5.ph1.x86_64.rpm , sha256 : a817ade4f1a18e3a8705f6a43af8b318ac87ad088e02f2eefd248acd7c278e82 , size : 90K , date : 2017-06-22  
bindutils-9.10.4-2.ph1.x86_64.rpm , sha256 : d8a3fea8f0541a41449a7192acff5ec5a35ba850c406ccd53b14adaf3e5b5fe4 , size : 3.0M , date : 2017-06-22   
bindutils-debuginfo-9.10.4-2.ph1.x86_64.rpm , sha256 : 2562ed6f2abcc4f08bc7828389131f5edcb91f929d2e2b5b1dfcab7e5009cc49 , size : 8.6M , date : 2017-06-22   
ruby-2.4.0-3.ph1.x86_64.rpm , sha256 : b4b427b77c549ce711a4c9d1fb3211693ec1d7be547b828725cf0e358298eee3 , size : 12M , date : 2017-06-22  
ruby-debuginfo-2.4.0-3.ph1.x86_64.rpm , sha256 : d6e2572ec5698749909433575f82cbbd9b6e9e79096cb77b19a645967913efa2 , size : 16M , date : 2017-06-22 
sudo-1.8.20p2-1.ph1.x86_64.rpm , sha256 : c71cb5749f915a8f7b63744b3a8567fd0f6d915477a19bafaaeeda21ac8c5388 , size : 1.5M , date : 2017-06-22     
sudo-debuginfo-1.8.20p2-1.ph1.x86_64.rpm , sha256 : d55ccc6f0cd8c876861867e9d34b9c297fd0fae4e90abe3857b6f74e4d6c69d9 , size : 653K , date : 2017-06-22     
krb5-1.14-6.ph1.x86_64.rpm , sha256 : d0a75ed4b3e8e5763cff03ec5cde42d817a332ced925996d53f8527e553abde3 , size : 1.4M , date : 2017-06-22   
krb5-debuginfo-1.14-6.ph1.x86_64.rpm , sha256 : fdc74faa89385a30995deebc8fdaf00dc0ae1e0b838fd55ed1ecfda43763804f , size : 4.6M , date : 2017-06-22   
