Download the Photon OS version that’s right for you. Click one of the links below.

**Selecting a Download Format**
----------------

Photon OS is available in the following pre-packaged, binary formats.
#### Download Formats ####
| Format | Description |
| --- | --- |
| ISO Image | Contains everything needed to install either the minimal or full installation of Photon OS. The bootable ISO has a manual installer or can be used with PXE/kickstart environments for automated installations. |
| OVA | Pre-installed minimal environment, customized for VMware hypervisor environments. These customizations include a highly sanitized and optimized kernel to give improved boot and runtime performance for containers and Linux applications. Since an OVA is a complete virtual machine definition, we've made available a Photon OS OVA that has virtual hardware version 11; this will allow for compatibility with several versions of VMware platforms or allow for the latest and greatest virtual hardware enhancements. |
| Amazon AMI | Pre-packaged and tested version of Photon OS made ready to deploy in your Amazon EC2 cloud environment. Previously, we'd published documentation on how to create an Amazon compatible instance, but, now we've done the work for you. |
| Google GCE Image | Pre-packaged and tested Google GCE image that is ready to deploy in your Google Compute Engine Environment, with all modifications and package requirements for running Photon OS in GCE. | 
| Azure VHD | Pre-packaged and tested Azure HD image that is ready to deploy in your Microsoft Azure Cloud, with all modifications and package requirements for running Photon OS in Azure. |

**Downloading Photon OS 2.0 Beta**
------------------------------
Photon OS 2.0 Beta is here! Choose the download that’s right for you and click one of the links below. Refer to the associated sha1sums and md5sums.
#### Photon OS 2.0 Beta Binaries ####
| Download | Size | sha1 checksum | md5 checksum |
| --- | --- | --- | --- |
| [Full ISO](https://bintray.com/vmware/photon/download_file?file_path=2.0%2FBeta%2Fiso%2Fphoton-2.0-8553d58.iso) | 2.1GB | 7a0e837061805b7aa2649f9ba6652afb2d4591fc | a52c50240726cb3c4219c5c608f9acf3 |
| [OVA with virtual hardware v11](https://bintray.com/vmware/photon/download_file?file_path=2.0%2FBeta%2Fova%2Fphoton-custom-hw11-2.0-8553d58.ova) | 110MB | 30b81b22a7754165ff30cc964b0a4a66b9469805 | fb309ee535cb670fe48677f5bfc74ec0 |
| [Amazon AMI](https://bintray.com/vmware/photon/download_file?file_path=2.0%2FBeta%2Fami%2Fphoton-ami-2.0-8553d58.tar.gz) | 136MB | 320c5b6f6dbf6b000a6036b569b13b11e0e93034 | cc3cff3cf9a9a8d5f404af0d78812ab4 |
| [Google GCE](https://bintray.com/vmware/photon/download_file?file_path=2.0%2FBeta%2Fgce%2Fphoton-gce-2.0-8553d58.tar.gz) | 705MB | c042d46971fa3b642e599b7761c18f4005fc70a7 | 03b873bbd2f0dd1401a334681c59bbf6 |
| [Azure VHD](https://bintray.com/vmware/photon/download_file?file_path=2.0%2FBeta%2Fazure%2Fphoton-azure-2.0-8553d58.vhd) | 17GB | 20cfc506a2425510e68a9d12ea48218676008ffe | 6a531eab9e1f8cba89b1f150d344ecab |

**Downloading Photon OS 1.0**
-------------------------

***Photon OS 1.0, Revision 2 Binaries (Update: January 19th, 2017)***
-------------------------------------------------------------
We've been busy updating RPMs in our repository for months, now, to address both functional and security issues. However, our binaries have remained fixed since their release back in September 2015. In order to make it faster and easier to get a up-to-date Photon OS system, we've repackaged all of our binaries to include all of these RPM updates. For clarity, we'll call these updated binaries, which are still backed by the 1.0 repos - **1.0, Revision 2**.

Choose the download that’s right for you and click one of the links below.
#### Photon OS 1.0, Revision 2 Binaries ####
| Download | Size | sha1 checksum | md5 checksum |
| --- | --- | --- | --- |
| [Full ISO](https://bintray.com/vmware/photon/download_file?file_path=photon-1.0-62c543d.iso) | 2.4GB | c4c6cb94c261b162e7dac60fdffa96ddb5836d66| 69500c07d25ce9caa9b211a8b6eefd61|
| [OVA with virtual hardware v10](https://bintray.com/vmware/photon/download_file?file_path=photon-custom-hw10-1.0-62c543d.ova) | 159MB | 6e9087ed25394e1bbc56496ae368b8c77efb21cb | 3e4b1a5f24ab463677e3edebd1ecd218|
| [OVA with virtual hardware v11](https://bintray.com/vmware/photon/download_file?file_path=photon-custom-hw11-1.0-62c543d.ova) | 159MB | 18c1a6d31545b757d897c61a0c3cc0e54d8aeeba| be9961a232ad5052b746fccbb5a9672d|
| [Amazon AMI](https://bintray.com/vmware/photon/download_file?file_path=photon-ami-1.0-62c543d.tar.gz) | 590MB | 6df9ed7fda83b54c20bc95ca48fa467f09e58548| 5615a56e5c37f4a9c762f6e3bda7f9d0|
| [Google GCE](https://bintray.com/vmware/photon/download_file?file_path=photon-gce-1.0-62c543d.tar.gz) | 164MB | 1feb68ec00aaa79847ea7d0b00eada7a1ac3b527| 5adb7b30803b168e380718db731de5dd|

There are a few other ways that you could create a Photon OS instance – either making the ISO from source that’s been cloned from the [GitHub Photon OS repository](https://github.com/vmware/photon), using the [instructions](https://github.com/vmware/photon/blob/master/docs/build-photon.md) found on the GitHub repo, using the [scripted installation](https://github.com/vmware/photon/blob/master/docs/kickstart.md), or [boot Photon OS over a network](https://github.com/vmware/photon/blob/master/docs/PXE-boot.md), using PXE. These options are beyond the scope of this document. If you’re interested in these methods, follow the links provided above. 

***Photon OS 1.0, Original Binaries***
--------------------------------

If you're looking for the original Photon OS, version 1.0 binaries, they can still be found here:
#### Photon OS 1.0, Original Binaries ####
| Download | Size | sha1 checksum | md5 checksum |
| --- | --- | --- | --- |
| [Full ISO](https://bintray.com/artifact/download/vmware/photon/photon-1.0-13c08b6.iso) | 2.1GB | ebd4ae77f2671ef098cf1e9f16224a4d4163bad1 | 15aea2cf5535057ecb019f3ee3cc9d34 |
| [OVA with virtual hardware v10](https://bintray.com/vmware/photon/download_file?file_path=photon-custom-hw10-1.0-13c08b6.ova) | 292MB | 8669842446b6aac12bd3c8158009305d46b95eac | 3ca7fa49128d1fd16eef1993cdccdd4d |
| [OVA with virtual hardware v11](https://bintray.com/vmware/photon/download_file?file_path=photon-custom-hw11-1.0-13c08b6.ova) | 292MB | 2ee56c5ce355fe6c59888f2f3731fd9d51ff0b4d | 8838498fb8202aac5886518483639073 |
| [Amazon AMI](https://bintray.com/artifact/download/vmware/photon/photon-ami-1.0-13c08b6.tar.gz) | 148.5MB | 91deb839d788ec3c021c6366c192cf5ac601575b | fe657aafdc8189a85430e19ef82fc04a |
| [Google GCE](https://bintray.com/artifact/download/vmware/photon/photon-gce-1.0-13c08b6.tar.gz) | 411.7MB | 397ccc7562f575893c89a899d9beafcde6747d7d | 67a671e032996a26d749b7d57b1b1887 |