## What Is Photon OS? 

Photon OS is an open source, Linux container host runtime optimized for VMware vSphere. Extensible and lightweight, Photon OS supports the most common container formats, including Docker, Rocket (rkt) and Garden.

Photon OS includes a small footprint, yum-compatible, package-based lifecycle management system--called tdnf. Photon OS alternatively supports image-based system versioning with RPM-OSTree.

When used with development tools and environments such as VMware Fusion, VMware Workstation, HashiCorp (Vagrant and Atlas) and production runtime environments such as VMware vSphere and vCloud Air, Photon OS lets you seamlessly migrate container-based applications from development to production.

## Introduction 
This document explains how to get started using Photon OS as a runtime environment for Linux containers by running Photon OS as a virtual machine on VMware vSphere.

Once Photon OS is installed, this document highlights how to deploy a containerized application in Docker with a single command.

## Photon OS Installation Prerequisites
In order to install and start using Photon OS with VMware vSphere, the following pre-requisites must be satisfied: 

* VMware vSphere 5.5 or VMware vSphere 6.0 installed 
* ESXi host with recommended 2GB of free RAM
* ESXi host with recommended 8GB of free disk space
* The Photon OS ISO downloaded from Bintray

This document uses VMware vSphere 6. VMware recommends that you use the latest version, though vSphere 5.5 or later should work as well. 

## Downloading Photon OS
This document covers installing Photon OS in vSphere from an OVA and from an ISO. Go to the following Bintray URL and download both the OVA and ISO for the latest release of Photon OS:

[https://bintray.com/vmware/photon/](https://bintray.com/vmware/photon/)

## Creating a Photon OS VM by Importing the OVA
Using the OVA is a fast and easy way to create a Photon OS VM. Once you’ve downloaded the OVA, log in to your vSphere environment and, from the `Actions` pull-down menu, select, `Deploy OVF Template …` On the popup window, point vSphere to the OVA file that you’ve downloaded. 

![vsphere1](https://cloud.githubusercontent.com/assets/11306358/9568101/1fc05610-4f06-11e5-912c-0009be3ef065.jpg)

Click the `Next` button at the bottom of the window and vSphere will upload and validate your OVA. Depending on bandwidth, this operation might take a while. 

After validating the image, vSphere will present a summary of the details of the OVA. Click the `Next` button to proceed to selecting a location to store the imported Photon OS instance.

Click `Finish`

At this point, you’ve got a Photon OS instance ready to go; but before you power on that Photon OS instance, consider first converting this VM into a template. By converting this imported VM to a template, you have a master Photon OS instance that can be combined with vSphere Guest Customization to enable rapid provisioning of Photon OS instances. 

The OVA contains a default password of "changeme" for the root account that must be changed upon initial login. For security, Photon OS forbids common dictionary words for the root password.
 
## Installing Photon OS on VMware vSphere from an ISO Image	
Once the ISO image has been uploaded to a datastore that is attached to the host on which you’ll create the Photon OS virtual machine, start the installation process by creating a new virtual machine.
When creating a new VM, the first thing you’ll need to specify is the compatibility of that VM with ESXi versions. Select a compatibility level for your VM, as shown below. Photon OS shouldn’t require any particular compatibility, but VMware recommends that you choose the latest available option for your release of vSphere.

![vsphere2](https://cloud.githubusercontent.com/assets/11306358/9568103/200be058-4f06-11e5-876c-8d3645abb638.jpg)

When prompted for the `Guest OS Family,` choose `Linux` and, for Guest OS Version, choose `Other 3.x Linux (64-bit)`. 

![vsphere3](https://cloud.githubusercontent.com/assets/11306358/9568111/21ae2920-4f06-11e5-97e6-3be30cea894b.jpg)

The recommended virtual hardware settings for your Photon VM are heavily dependent upon the container load you intend to run within Photon OS – more containers or more intensive containers will require you to adjust these settings for your application load. VMware suggests 2 vCPU, 1024MB memory, 20GB hard disk. Any unwanted devices should be removed.  Be sure to mount the Photon OS ISO on the CD/DVD Drive and put a check in the box next to, `Connect At Power On.`

![vsphere4](https://cloud.githubusercontent.com/assets/11306358/9568107/2089147e-4f06-11e5-9549-908b8ab21a86.jpg)

To summarize, these are the settings we recommend as a starting point for your Photon OS container runtime host: Thin provisioned, hardware compatibility: ESXi 6.0 and later (VM version 11).

Power on the Photon OS virtual machine and, within a few seconds, the Photon Installer Boot Menu will appear.  Download and install the Remote Console if you do not have it already; otherwise, click `Launch Remote Console` to interact with the installer.

![vsphere5](https://cloud.githubusercontent.com/assets/11306358/9568105/20589cd6-4f06-11e5-8b6c-88974382317d.jpg)

Once connected to the remote console, select `Install` to proceed.

![vsphere6](https://cloud.githubusercontent.com/assets/11306358/16130444/b781bcac-33ce-11e6-8cbf-2dea3c0e3e40.png)

After you accept the EULA, the installer will detect one disk, which should be the 20GB volume configured as part of the virtual machine creation. Select the disk and press enter.  When you are prompted to confirm that it is okay to erase the entire disk, select `Yes` to accept and proceed with the installation.

![vsphere7](https://cloud.githubusercontent.com/assets/11306358/9568104/2043ea16-4f06-11e5-9b8f-48f6037501da.jpg)

You will now be presented with several installation options:

![vsphere8](https://cloud.githubusercontent.com/assets/11306358/16130445/b787f3e2-33ce-11e6-874b-ebdf97b568b5.png)

Each install option provides a different runtime environment:

* Photon Minimal: Photon Minimum is a very lightweight version of the container host runtime that is best suited for container management and hosting. There is sufficient packaging and functionality to allow most common operations around modifying existing containers, as well as being a highly performant and full-featured runtime. 

* Photon Full: Photon Full includes several additional packages to enhance the authoring and packaging of containerized applications and system customization. For simply running containers, Photon Full will be overkill. Use Photon Full for developing and packaging the application that will be run as a container, as well as authoring the container itself. For testing and validation purposes, Photon Full will include all components necessary to run containers. 

* Photon OSTree Host: This installation profile creates a Photon OS instance that will source its packages from a central rpm-ostree server and continue to have the library and state of packages managed by the definition that is maintained on the central rpm-ostree server. Use Photon OStree Hosts when you are interested in experimenting with the concept of a centrally authored and maintained OS version. This concept of treating the OS as a versioned, atomic entity can simplify lifecycle management and security at scale. 

* Photon OSTree Server: This installation profile will create the server instance that will host the filesystem tree and managed definitions for rpm-ostree managed hosts created with the Photon OSTree Host installation profile. Most environments should need only one Photon OSTree Server instance to manage the state of the Photon OSTree Hosts. Use Photon OSTree Server when you are establishing a new repository and management node for Photon OS hosts.  

For the purposes of this how-to guide, select the option to install Photon Minimal. Once `Photon Minimal` is highlighted, press the Enter key on your keyboard.

You will now be prompted for a hostname. Photon OS will prepopulate a randomly generated, unique hostname; you can either use this suggestion or enter your own hostname, as shown in the screenshot below:

![vsphere9](https://cloud.githubusercontent.com/assets/11306358/9568108/20a8348a-4f06-11e5-9826-fcf992fb6635.jpg)

After selecting a hostname and pressing Enter, you will be prompted to first type and, then, confirm the system root password. If you have trouble with unintentional repeated characters in the Remote Console, follow VMware KB 196 (http://kb.vmware.com/kb/196) for a setting to apply to the virtual machine.

*Note: Photon OS will not permit commonly used dictionary words to be set as a root password.*

After confirming the password, the installation process should begin.

Installation times will vary based on system hardware and installation options, but most installations complete in less than a minute. Once the installation completes, press any key and the virtual machine will reboot into Photon OS.

As the initial boot process begins, you will see the Photon splash screen before you are taken to a login prompt.

At the login prompt, enter `root` as the username and provide the password chosen during the installation. 

You have now successfully setup Photon OS and are ready to use your container runtime environment.
 
## Installing a Containerized Application to Help Demonstrate Capability
Now that you have your container runtime environment up and running, you may be wondering, “what can I do now?” A command prompt is not the most exciting thing. To help demonstrate the ease with which you can deploy a containerized application, this section showcases how you can quickly get a web server up and running.

For this example, we will use the popular open source web server Nginx. The Nginx application has a customized VMware package  published as a dockerfile that can be downloaded directly in Docker from the Docker Hub.

To run Docker from the command prompt, enter the command below to initialize the docker engine:

`systemctl start docker`

To ensure the docker daemon service runs on every subsequent VM reboot, enter:

`systemctl enable docker`

Now that the docker daemon service is running, it is a simple task to pull and start the Nginx Web Server container from Docker Hub.  To do this, enter the following command:

`docker run -d -p 80:80 vmwarecna/nginx`

This will then pull the Nginx Web Server files and appropriate dependent containers to ensure this containerized application can run.  You will see a screenshot similar to below, as the container and dependencies are downloaded and the container is prepared to run: 

![vsphere12](https://cloud.githubusercontent.com/assets/11306358/9568112/21aeeedc-4f06-11e5-9feb-280e4a8f2d5b.jpg)

Once the `docker run` process is completed, you will be returned to the command prompt.  You now have a fully active web server up and running through typing just a single command within Photon OS using containers.

To test that your web server is active, we need to get the IP address of the Photon OS Virtual Machine. To get the IP address, enter the following command: 

	ifconfig

This will now display a list of adapters connected to the virtual machine.  Typically, the web server daemon will be bound to `eth0.`  

Start a browser on your host machine and enter the IP address of your Photon OS Virtual Machine.  The following screen will appear showing that your web server is active:

![vsphere13](https://cloud.githubusercontent.com/assets/11306358/9568113/21b1990c-4f06-11e5-9136-afe1e45bd105.jpg)

You can now run any other containerized application from Docker Hub or your own containerized application within Photon OS.

If you are having trouble viewing the screen that shows the web server is active, you might have to add an iptables rule to your container VM to allow traffic on Port 80:

`iptables -I INPUT 1 -p tcp --dport 80 -j ACCEPT`

In addition, you might have to modify /etc/httpd/httpd.conf to listen on Port 80 by adding the following line:  

`Listen 0.0.0.0:80`

**We hope you enjoy using Photon OS as much as we enjoy creating it.**
