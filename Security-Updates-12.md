# [Critical] Photon OS Security Update


## Summary


Advisory ID: PHSA-2016-0012    
Type : Security   
Severity : [ Critical , Moderate ]    
Issued on : 2016-Dec-06    
Affected versions : PhotonOS 1.0    

## Details
An update of [ linux , wget , vim , grub2 , zookeeper , nginx , dnsmasq , haproxy  ] packages for PhotonOS has been released.

## Affected Packages:

[Critical]     
linux - [CVE-2016-9555](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-9555)   

[Moderate]    
linux - [CVE-2016-9083](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-9083)    
wget - [CVE-2016-7098](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7098)    
vim - [CVE-2016-1248](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-1248)    
grub2 - [CVE-2015-8370](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-8370)   
zookeeper - [CVE-2016-5017](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5017)     
nginx - [CVE-2016-4450](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-4450)      
dnsmasq - [CVE-2015-8899](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-8899)    
haproxy - [CVE-2016-5360](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5360)    

## Solution
Install the update packages ( tdnf update _package_name_ )    

Note: For package [ linux ] after updating, a reboot is required for taking effect.    

## Updated Packages Information
linux-api-headers-4.4.35-1.ph1.noarch.rpm , sha256 : a9f5313e01dfa9d8c01de553ccc935390a6aa610afedd649e9d78ab84035da10 , size : 1.1M , date : 2016-12-06    
linux-4.4.35-1.ph1.x86_64.rpm , sha256 : d9b60dfa09d786e0762d7e419bb70dcbdaf7687018380818162c7a7fd5279677 , size : 19M , date : 2016-12-06     
linux-debuginfo-4.4.35-1.ph1.x86_64.rpm , sha256 : b0d5c4a0053bc535789ca3cdf7f83f3131b2d4102748b7dbb1b94635b3639f05 , size : 347M , date : 2016-12-06      
linux-dev-4.4.35-1.ph1.x86_64.rpm , sha256 : ea8b6e957ccfe73c340a0965769d6e45aee3ab5353b6f22e474375f3f535bcd9 , size : 9.9M , date : 2016-12-06    
linux-docs-4.4.35-1.ph1.x86_64.rpm , sha256 : f87e0cc327cb5258131d7ea8a4ff531e2e53c1c21ec29e1fac97acaeb6dbd084 , size : 6.6M , date : 2016-12-06       
linux-drivers-gpu-4.4.35-1.ph1.x86_64.rpm , sha256 : de138dbc22bb172cbcd6765990d8ae02ae7de1416b516c425e65ed50c9e3fcb3 , size : 1.7M , date : 2016-12-06    
linux-esx-4.4.35-1.ph1.x86_64.rpm , sha256 : ee65fc991ad491c58f0375924e221c32ab2e85b89a1e6252fe63636fa33b835c , size : 7.0M , date : 2016-12-06     
linux-esx-debuginfo-4.4.35-1.ph1.x86_64.rpm , sha256 : 914d90cb2e67204d1fbc84d0ff1da897c2052145713ef6178618fa324e086152 , size : 4.1M , date : 2016-12-06     
linux-esx-devel-4.4.35-1.ph1.x86_64.rpm , sha256 : 1f3381ee31e55f22c79c2dc2afdd3a62b8d30f75398dc70cf36a1be1b64e740a , size : 9.7M , date : 2016-12-06      
linux-esx-docs-4.4.35-1.ph1.x86_64.rpm , sha256 : 859b2b4ef622d204b119125447666d717a87c7e600fa15a1de626d024fec3957 , size : 6.6M , date : 2016-12-06       
linux-oprofile-4.4.35-1.ph1.x86_64.rpm , sha256 : 59cf876760f94f5bf38185f045f9e29673a0bc87983a1d5429e3e304f1e3fe88 , size : 34K , date : 2016-12-06        
linux-sound-4.4.35-1.ph1.x86_64.rpm , sha256 : 61bb28a47cd6ebbabf57e877a3352cd04f980ad90d93bad799b0b0904ce570ce , size : 262K , date : 2016-12-06         
linux-tools-4.4.35-1.ph1.x86_64.rpm , sha256 : f159905a22e9ea3ebe00d33e7460682a1aaa39e4601ca26b1d18601cdb33e885 , size : 703K , date : 2016-12-06                
linux-tools-debuginfo-4.4.35-1.ph1.x86_64.rpm , sha256 : 826d15f81b175b6eab4756041e9967b150b655490d31ef3831d3d77ff0860dba , size : 3.4M , date : 2016-12-06           
wget-1.18-1.ph1.x86_64.rpm , sha256 : ebe864240acb7d5b69345db9568bcb1e298b5b399c82a494825518fa322acedd , size : 873K , date : 2016-12-06       
wget-debuginfo-1.18-1.ph1.x86_64.rpm , sha256 : b520cf3bcbcea9b7702f9f6d78799d0f4da773d3c4ed295dda008a97e380924f , size : 903K , date : 2016-12-06        
vim-7.4-6.ph1.x86_64.rpm , sha256 : 813044e145b0378aef94dabfb2ba5cb3015492eceac2168fb3669323bcfc1bf1 , size : 1022K , date : 2016-12-06         
vim-extra-7.4-6.ph1.x86_64.rpm , sha256 : f47795dd779c8fc739ee9a9705d48d9775d40a87f6268872ef4128213ab9f36d , size : 8.3M , date : 2016-12-06            
grub2-2.02-5.ph1.x86_64.rpm , sha256 : 5b9cf3b866040e69dbd7dd34025b40967ba5632833f9c0de6ef216edd776946b , size : 9.4M , date : 2016-12-06       
grub2-efi-2.02-3.ph1.x86_64.rpm , sha256 : f38a6d5a2d400c30ac6176590c50df94c5b7a6126ef9a8d56b80abdb93035541 , size : 9.9M , date : 2016-12-06      
grub2-efi-lang-2.02-3.ph1.x86_64.rpm , sha256 : e5c50e63dd193ad5fe9f364c96a7cf8f4f5ab108254ce1ef461ea35b92c8177d , size : 1.1M , date : 2016-12-06      
grub2-lang-2.02-5.ph1.x86_64.rpm , sha256 : c14c52b0ec4aa45cb0b2a98b5e9c1c14d64c1e8293bf4c4ab7bc546cf61b8880 , size : 1.1M , date : 2016-12-06       
zookeeper-3.4.9-1.ph1.x86_64.rpm , sha256 : a0dbef675ce5ae29266074d6a912a174ceae0d02efe2a33633c9026b9fc79d8e , size : 3.0M , date : 2016-12-06          
nginx-1.10.0-4.ph1.x86_64.rpm , sha256 : f7ffb60fda5be8d8601cd8c89130f434b49042762685683a4c9e21739d547970 , size : 2.5M , date : 2016-12-06         
nginx-debuginfo-1.10.0-4.ph1.x86_64.rpm , sha256 : 5020fb3b768772693f26a358f65367da309b78d49dfa5d73aac281c3a87fa330 , size : 2.7M , date : 2016-12-06         
dnsmasq-2.76-1.ph1.x86_64.rpm , sha256 : 549838eb11b7ff48d0e62a425540fe09c238dd6bcbb8cb4bcf26f708fd69d95a , size : 213K , date : 2016-12-06     
dnsmasq-debuginfo-2.76-1.ph1.x86_64.rpm , sha256 : c67be01b5c39ff5d4e9083d96cc26016ac71c6cdf001832ab9ae259517653377 , size : 14K , date : 2016-12-06       
haproxy-1.6.10-1.ph1.x86_64.rpm , sha256 : 45bb425aa607d7813e8b37e0a558a999fa327252b96f4b9bc9076466855d1867 , size : 440K , date : 2016-12-06        
haproxy-debuginfo-1.6.10-1.ph1.x86_64.rpm , sha256 : 973fdee9fab261a6d436bc5abc84ce796734d6d0d2c7b844d78ed232f9f40996 , size : 2.7M , date : 2016-12-06        
haproxy-doc-1.6.10-1.ph1.x86_64.rpm , sha256 : 06a6d4e6621370c154edcf90dc26f9042de448c7914fd5aed60d8c4dcf843bee , size : 304K , date : 2016-12-06      
