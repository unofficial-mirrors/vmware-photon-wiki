# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0006    
Type : Security   
Severity : [ Critical ]   
Issued on : 2017-Feb-27   
Affected versions : PhotonOS 1.0   

## Details
An update of [linux,vim] packages for PhotonOS has been released.    

## Affected Packages:    

### [Critical]
linux - [CVE-2017-5986](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5986) [CVE-2017-6074](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6074)    
vim - [CVE-2017-5953](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5953)    

## Solution

Install the update packages ( _tdnf update package_name_ )

Note: For package [ linux ] after updating, a reboot is required for taking effect.   

## Updated Packages Information
linux-api-headers-4.4.51-1.ph1.noarch.rpm , sha256 : 38a17a167bb85075f88ffb71cd1018592e28e0dabcea5912bbb2ba12517c3825 , size : 1.1M , date : 2017-02-27   
linux-4.4.51-1.ph1.x86_64.rpm , sha256 : 96787c2a5248ee3b215c64efe310034f236e55fcea9691d4e016dbe3335e389e , size : 17M , date : 2017-02-27   
linux-debuginfo-4.4.51-1.ph1.x86_64.rpm , sha256 : 0a69ef1605dd033f9f357bae5240237a13b80874b85eeccec74e4b4819148196 , size : 336M , date : 2017-02-27   
linux-dev-4.4.51-1.ph1.x86_64.rpm , sha256 : 16e2f6bdce47d72669787ecdc2114c4ab4f5c404cedc1464e1ee6cec49d21704 , size : 9.9M , date : 2017-02-27   
linux-docs-4.4.51-1.ph1.x86_64.rpm , sha256 : 6291ef24cfd3d1f9ec44eb166458a5623ca3cbdf42b2682de35de5fd0f7d3f5b , size : 6.6M , date : 2017-02-27   
linux-drivers-gpu-4.4.51-1.ph1.x86_64.rpm , sha256 : 7c5d909d37ff29782e82122cad608f3de148eee5826649e13b25575e33cb4f97 , size : 1.4M , date : 2017-02-27   
linux-esx-4.4.51-1.ph1.x86_64.rpm , sha256 : a8891b7e38fe9def4396eec2c018d3d78ab5c2534c7581f1b1fd1bf22545e36b , size : 7.0M , date : 2017-02-27  
linux-esx-debuginfo-4.4.51-1.ph1.x86_64.rpm , sha256 : a7d0defd97e7830e75a67646e2ad9b1eb8e0ea30111d3c352db0bbfec82f7661 , size : 4.1M , date : 2017-02-27  
linux-esx-devel-4.4.51-1.ph1.x86_64.rpm , sha256 : 767dff4bda914343027a802ce80b8390c99fc3ea88a8a7ffe64bb0c3ec297de0 , size : 9.7M , date : 2017-02-27   
linux-esx-docs-4.4.51-1.ph1.x86_64.rpm , sha256 : 80276e62cc0800f9054ec4ae59730a01c13cc5ddb12c72309d76dbfc29fc18fe , size : 6.6M , date : 2017-02-27   
linux-oprofile-4.4.51-1.ph1.x86_64.rpm , sha256 : a388b8da87947fcddec420110c2e29dca9069e19f0b415b635b7f1998afa5450 , size : 30K , date : 2017-02-27   
linux-sound-4.4.51-1.ph1.x86_64.rpm , sha256 : 7677cf8ba5a3b8b8b9e70d1a1737f7126b7d0027ae76b4ac7e5181a4a4fc60bc , size : 218K , date : 2017-02-27   
linux-tools-4.4.51-1.ph1.x86_64.rpm , sha256 : 4c3c11e3248ba191c76a8545c66ab73e11b2e120f24a897fc597135ded5a934e , size : 704K , date : 2017-02-27   
linux-tools-debuginfo-4.4.51-1.ph1.x86_64.rpm , sha256 : a796fdaeeaedcd42d18f21702033ccb53f828672bc3c2ac0ee7fb89d202bd856 , size : 3.4M , date : 2017-02-27      
vim-7.4-7.ph1.x86_64.rpm , sha256 : dcb10f12625b0f11964928c4f26a4bb392e19d3b8b01261014534249b47b5100 , size : 1022K , date : 2017-02-27    
vim-extra-7.4-7.ph1.x86_64.rpm , sha256 : cb4087792ff82b66437523900ab1dc411fe9300691f90ea7576754b298dbe3b5 , size : 8.3M , date : 2017-02-27     
