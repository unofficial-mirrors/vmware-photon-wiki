# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0013   
Type : Security    
Severity : [Critical,Moderate]  
Issued on : 2017-Apr-24   
Affected versions : PhotonOS 1.0   

## Details
An update of [cracklib,libevent,libgcrypt,httpd,glibc] packages for PhotonOS has been released.

## Affected Packages:

### [Critical]
libevent - [CVE-2016-10195](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-10195)

### [Moderate]
cracklib - [CVE-2016-6318](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-6318)   
libgcrypt - [CVE-2016-6313](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-6313)   
httpd - [CVE-2016-1546](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-1546)   
libevent - [CVE-2016-10196](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-10196) [CVE-2016-10197](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-10197)     
glibc - [CVE-2016-3706](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-3706) [CVE-2016-4429](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-4429) [CVE-2016-3075](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-3075) [CVE-2016-1234](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-1234)
## Solution
Install the update packages ( _tdnf update package_name_ )

## Updated Packages Information
cracklib-2.9.6-3.ph1.x86_64.rpm , sha256 : ca4d6adb92ccd036ba1fb287a264c4b52588c72cb2290513591c1400dda537cb , size : 32K , date : 2017-04-24    
cracklib-debuginfo-2.9.6-3.ph1.x86_64.rpm , sha256 : c5ed3ec2bd685b1a0e41a920ea49b7f617d32f23a9f1d2b643259514a1cff615 , size : 54K , date : 2017-04-24    
cracklib-devel-2.9.6-3.ph1.x86_64.rpm , sha256 : a1d3d416b9360fb512452d8e77dabe0fc1416f8f96a265e2c1141301d46ad082 , size : 20K , date : 2017-04-24    
cracklib-dicts-2.9.6-3.ph1.x86_64.rpm , sha256 : f1070e02e856a218f9486d91b80af1fd7e26340ebc88bd49589f89f05b960366 , size : 4.1M , date : 2017-04-24    
cracklib-lang-2.9.6-3.ph1.x86_64.rpm , sha256 : e4018668652c730390bcf86014974c15fe330e0f7496008de697b2d381b72a26 , size : 29K , date : 2017-04-24    
cracklib-python-2.9.6-3.ph1.x86_64.rpm , sha256 : a9893d53847b6c2dfe2e621724c893923df19dcb8be48b5a77f39fe879a812d6 , size : 15K , date : 2017-04-24   
libevent-2.1.8-1.ph1.x86_64.rpm , sha256 : fff9d54f5d8c2f0388fa1ebe1f5a80201667593bb93f7c878c4f2241ba47c9a1 , size : 320K , date : 2017-04-24    
libevent-debuginfo-2.1.8-1.ph1.x86_64.rpm , sha256 : 14f4e93888b88a0e7a299644ffc1a065c16b901067c01290b118daa12457bede , size : 1.1M , date : 2017-04-24   
libevent-devel-2.1.8-1.ph1.x86_64.rpm , sha256 : 662d458e357442199e890c75493b4077a9cd95b598250d77088ec39948c19f6e , size : 101K , date : 2017-04-24   
libgcrypt-1.7.6-1.ph1.x86_64.rpm , sha256 : 9df577ce1b1b373a460640c7eaa78e8dc5b8cd4182f1406fb3cf660ce926d798 , size : 457K , date : 2017-04-24   
libgcrypt-debuginfo-1.7.6-1.ph1.x86_64.rpm , sha256 : ab82e54fcfe93cdca2ab437b5bca3ac2ebb06611f0a602a35c29f58ec54cf7a7 , size : 1.6M , date : 2017-04-24   
libgcrypt-devel-1.7.6-1.ph1.x86_64.rpm , sha256 : b521319a88db6a4ddc89bd33ad94d04c76e9d1712aea73b6ae9186c46ca7ec19 , size : 23K , date : 2017-04-24   
httpd-2.4.25-2.ph1.x86_64.rpm , sha256 : 998b6ca7a0ca184c3f319fcdf4b6b0ca3d850666fc232af7c30572816b60d319 , size : 4.8M , date : 2017-04-24   
httpd-debuginfo-2.4.25-2.ph1.x86_64.rpm , sha256 : 66b9967cef7f3dd467cef177dec3297bffba5e97ce84205395033b071bd9d33a , size : 4.8M , date : 2017-04-24   
httpd-devel-2.4.25-2.ph1.x86_64.rpm , sha256 : aee332a584ac8fd24f72a6e594d86fc5d7cdb8471e66c54c84d79389966d0ae8 , size : 183K , date : 2017-04-24    
httpd-docs-2.4.25-2.ph1.x86_64.rpm , sha256 : 078abba9345fd95b99be7bb20d59499af7d977e399ebc66c3c650822a98ac2eb , size : 5.1M , date : 2017-04-24    
httpd-tools-2.4.25-2.ph1.x86_64.rpm , sha256 : c13fd9091d2fa0244864dde1f8d1982c7f326b14a5ca8da6943cbc15d62f4302 , size : 14K , date : 2017-04-24   
glibc-2.22-10.ph1.x86_64.rpm , sha256 : 667f2bf128182c10e9091181775f9df8240934c8798ecf89ef801a4d149989c2 , size : 19M , date : 2017-04-24   
glibc-devel-2.22-10.ph1.x86_64.rpm , sha256 : 7a3ed971de2f94a34061c7307d8e940eed6581ac7cd1d3a6225fca116a75e052 , size : 9.8M , date : 2017-04-24   
glibc-lang-2.22-10.ph1.x86_64.rpm , sha256 : 6a13dd59c43f06e07e2bb743ebb2c2ea38fe80eca4243a22e02504cbc26aa935 , size : 1.4M , date : 2017-04-24    
   
