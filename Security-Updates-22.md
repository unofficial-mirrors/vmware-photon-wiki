# [Moderate] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0005   
Type : Security   
Severity : [ Moderate ]   
Issued on : 2017-Feb-16   
Affected versions : PhotonOS 1.0   

## Details
An update of [systemd] package/s for PhotonOS has been released.

## Affected Packages:

### [Moderate]
systemd - [CVE-2016-10156](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-10156)

## Solution
Install the update packages ( _tdnf update package_name_ )

Note: For package [ systemd ] after updating, a reboot is required for taking effect.   
## Updated Packages Information     
systemd-228-34.ph1.x86_64.rpm , sha256 : ef46c24a05078ca0b24da4675a8da98f3d78c141f1ececf476adfb4074617562 , size : 12M , date : 2017-02-16     
systemd-debuginfo-228-34.ph1.x86_64.rpm , sha256 : 8df03ff1741343ee0ee066595de9acda5c70ee8a2e920a67e4a385ea3bdf8a4a , size : 57M , date : 2017-02-16   
