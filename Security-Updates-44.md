# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0018   
Type : Security  
Severity : [Critical,Moderate]  
Issued on : 2017-May-31   
Affected versions : PhotonOS 1.0   

## Details
An update of [linux,libxlt] packages for PhotonOS has been released. 

## Solution
Install the update packages ( _tdnf update package_name_ )   
Note: For package [ linux ] after updating, a reboot is required for taking effect.

## Affected Packages:

### [Critical]
linux - [CVE-2017-7487](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-7487)   

## [Moderate]
linux - [CVE-2017-9059](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9059)   
libxlt - [CVE-2017-5029](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5029)   

## Updated Packages Information
linux-api-headers-4.4.70-1.ph1.noarch.rpm , sha256 : a9cf7ed06a18158845e99e751caad14fc5a390712dc698dc1fca0383c044c74d , size : 1.1M , date : 2017-05-31   
linux-4.4.70-1.ph1.x86_64.rpm , sha256 : f022767128f4b38251c93a4fb3e526a5cc8fac1aecddce7663e34f145d014906 , size : 17M , date : 2017-05-31   
linux-debuginfo-4.4.70-1.ph1.x86_64.rpm , sha256 : f3be8d3ef4dbc8fb336e1e4d4c33df92d457a46face203fba7b8383dc07edde7 , size : 340M , date : 2017-05-31   
linux-dev-4.4.70-1.ph1.x86_64.rpm , sha256 : fd0bdce161f3d3b2bc9af7aec5de86d3092730979b8d942d26f6280ee3c2c437 , size : 9.9M , date : 2017-05-31   
linux-docs-4.4.70-1.ph1.x86_64.rpm , sha256 : 658d9287f8bb9562baa32daf330f1f83a410929a7ebba0c8f90011d27053d75c , size : 6.6M , date : 2017-05-31  
linux-drivers-gpu-4.4.70-1.ph1.x86_64.rpm , sha256 : 2ac655f9b5fc17df06789ca33f845d41ce6c56b94e423a249a7a4c370737aceb , size : 1.4M , date : 2017-05-31  
linux-esx-4.4.70-1.ph1.x86_64.rpm , sha256 : 17a45e97393a88b9e2e30682f4ec76c256f2c4096372fa1a9982e076f4c5f379 , size : 7.0M , date : 2017-05-31   
linux-esx-debuginfo-4.4.70-1.ph1.x86_64.rpm , sha256 : 8ed7de6aaec04cd2bde9397d00d710d3932cb40b7b94ac9da68987bd7b834b6f , size : 154M , date : 2017-05-31   
linux-esx-devel-4.4.70-1.ph1.x86_64.rpm , sha256 : 2388d032a9d6efbf98226b44fb33e0a358dff42dc41fb068020d1fd8be434d49 , size : 9.7M , date : 2017-05-31   
linux-esx-docs-4.4.70-1.ph1.x86_64.rpm , sha256 : 558934fa05dea84398fda9bd395be7d9f8d77287180b38d0afe22529d3e1d666 , size : 6.6M , date : 2017-05-31   
linux-oprofile-4.4.70-1.ph1.x86_64.rpm , sha256 : aa491cdf4507de7ba500b72469ee8dd3123e7a077db0c260ab44fa8c340d4a20 , size : 32K , date : 2017-05-31   
linux-sound-4.4.70-1.ph1.x86_64.rpm , sha256 : f3383e625552cad3b4d44a85cfb349f039536fbd35681be316397972ddef48ed , size : 226K , date : 2017-05-31    
linux-tools-4.4.70-1.ph1.x86_64.rpm , sha256 : 22c4d5c19eb3fc481f3d825d8e91e63d7f487917632b72cbf04e1a82221c6e43 , size : 687K , date : 2017-05-31   
