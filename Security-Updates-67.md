# [Important] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0033  
Type : Security   
Severity : [Important]   
Issued on : 2017-Sep-13    
Affected versions : PhotonOS 1.0   

## Details
An update of [tcpdump] packages for PhotonOS has been released.  

## Affected Packages:
### [Important]
tcpdump - [CVE-2017-11541](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-11541) [CVE-2017-11542](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-11542) [CVE-2017-11543](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-11543)

## Solution
Install the update packages ( tdnf update package_name )   
## Updated Packages Information
tcpdump-4.9.1-2.ph1.x86_64.rpm , sha256 : 08facbd69adc715b19a833e186a5acc12570bdd107466e951016c0ac8dded981 , size : 859K , date : 2017-09-13    
tcpdump-debuginfo-4.9.1-2.ph1.x86_64.rpm , sha256 : 6832cbdd0607aba8f217a6f30f9aed86751dc81c01c05ea68ca7714403523416 , size : 2.3M , date : 2017-09-13  
