# [Important] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0031  
Type : Security   
Severity : [Important]   
Issued on : 2017-Aug-30  
Affected versions : PhotonOS 1.0   

## Details
An update of [subversion] packages for PhotonOS has been released.     

## Affected Packages:
### [Important]
subversion - [CVE-2017-9800](https://nvd.nist.gov/vuln/detail?vulnId=CVE-2017-9800)   

## Solution
Install the update packages ( tdnf update package_name )  

## Updated Packages Information
subversion-1.9.4-2.ph1.x86_64.rpm , sha256 : 06a9de3e80ed31d3e8f5616c314118ce0723329dfbecbdaaeae24e96eba466bd , size : 2.7M , date : 2017-08-30   
subversion-debuginfo-1.9.4-2.ph1.x86_64.rpm , sha256 : 1eae54bcb8344780b7700eb4f86639f55039cba553400cece39d9200659d0646 , size : 7.4M , date : 2017-08-30   
subversion-devel-1.9.4-2.ph1.x86_64.rpm , sha256 : f44976288dd93329a28d7460e459dca137d86688ab37bd03b0d2c9d92af4d00b , size : 347K , date : 2017-08-30  
