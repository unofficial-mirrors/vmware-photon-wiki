# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0023   
Type : Security  
Severity : [Critical,Moderate,Low]   
Issued on : 2017-Jul-10  
Affected versions : PhotonOS 1.0   

## Details
An update of [systemd,wget,shadow,glibc] packages for PhotonOS has been released.

## Affected Packages:

### [Critical]
glibc - [CVE-2017-1000366](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-1000366)   

### [Moderate]
systemd - [CVE-2017-9445](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-9445)   

### [Low]
wget - [CVE-2017-6508](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6508)   
shadow - [CVE-2016-6252](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-6252)   

## Solution
Install the update packages ( tdnf update package_name )   
Note: For package [ systemd ] after updating, a reboot is required for taking effect.

## Updated Packages Information
systemd-228-35.ph1.x86_64.rpm , sha256 : 4d440eb976dcb208a604a62bb5881375f5f8fe4c4469f57e12126080f4659835 , size : 12M , date : 2017-07-10    
systemd-debuginfo-228-35.ph1.x86_64.rpm , sha256 : 5f040f09d34a70a67d0ca684a6ab31b438915e6e954c7f70e2108baff30b26bb , size : 57M , date : 2017-07-10   
wget-1.18-2.ph1.x86_64.rpm , sha256 : aebdfa6de0ae86cad0ae3b3dad94a2f3bf364cdd8b99ae35bf2a31a116eef253 , size : 874K , date : 2017-07-10    
wget-debuginfo-1.18-2.ph1.x86_64.rpm , sha256 : 671aebcda996f70390ad8810e1607ace4eaff0d0c7c4406f32f4db8b72b8c3cc , size : 904K , date : 2017-07-10    
shadow-4.2.1-10.ph1.x86_64.rpm , sha256 : 7de7c0ed00ba5346d2d4347172be0061675d4da32ab4c2752f7d137edd49d340 , size : 2.0M , date : 2017-07-10   
shadow-debuginfo-4.2.1-10.ph1.x86_64.rpm , sha256 : 78780189e37dc5419464957e13623d0f5e04dd37f4f5b22ff6691e1e7f019843 , size : 1.7M , date : 2017-07-10   
glibc-2.22-13.ph1.x86_64.rpm , sha256 : 455bf4c2f6543b0e65ac689da0d97d3ef2fd2002dd48c18ea3ab0b9c0e6ab236 , size : 19M , date : 2017-07-10   
glibc-devel-2.22-13.ph1.x86_64.rpm , sha256 : a40d347f17db8475ecad1e75ade78045a49ee77d9bac8a20ef4cd8850e8ab345 , size : 9.8M , date : 2017-07-10  
glibc-lang-2.22-13.ph1.x86_64.rpm , sha256 : 2915d3ea1262f57f802c0ce0a801352e94cb85f10c6fae826abab6565c2845e9 , size : 1.4M , date : 2017-07-10   
