# [Important] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0030   
Type : Security   
Severity : [Important]   
Issued on : 2017-Aug-18   
Affected versions : PhotonOS 1.0   

## Details
An update of [rsyslog,shadow] packages for PhotonOS has been released.   

## Affected Packages:
### [Important]
rsyslog - [CVE-2017-12588](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-12588)    
shadow - [CVE-2017-12424](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-12424)

## Solution
Install the update packages ( tdnf update package_name )   



## Updated Packages Information
rsyslog-8.15.0-6.ph1.x86_64.rpm , sha256 : d5ff620765c6257ef9cb126ba60c852777608c0cb6446cb10424afe9968b32f9 , size : 629K , date : 2017-08-18   
rsyslog-debuginfo-8.15.0-6.ph1.x86_64.rpm , sha256 : acb5110980828749adbc9bb1a2911545ff512d99d3c351b1b866343115f159e3 , size : 2.8M , date : 2017-08-18   
shadow-4.2.1-11.ph1.x86_64.rpm , sha256 : 4cccbaec3f3ffc3404df1bb4ca25f6c012500667663e48767c38ef69095403d3 , size : 2.0M , date : 2017-08-18   
shadow-debuginfo-4.2.1-11.ph1.x86_64.rpm , sha256 : 2d903df93922c127c6fb9823a1b9149f5f212d243252a204ad7698ad43a37c14 , size : 1.7M , date : 2017-08-18  
