# What is Photon OS?

Photon OS is an open source, Linux container host runtime environment optimized for VMware vSphere. Photon OS is an extensible, lightweight operating system that supports the most common container formats, including Docker, Rocket (rkt), and Garden.

Photon OS includes a small footprint, yum-compatible, package-based lifecycle management system -- called tdnf -- and, alternatively, supports an rpm-ostree image-based system versioning.

When used with development tools and environments such as VMware Fusion, VMware Workstation, Vagrant and Atlas from HashiCorp, and production runtime environments like vSphere and vCloud Air, Photon OS empowers you to seamlessly migrate container-based applications from development to production.  

# Introduction

This document explains how to get started using Photon OS as a runtime environment for Linux containers by running Photon OS as a VM instance on Google Compute Engine (GCE). Once Photon OS is installed as an image on GCE, this guide highlights how simple it is to deploy a containerized application with Docker by installing a web server with a single command.

# Prerequisites for Photon OS on GCE
The following items are required for preparing and launching Photon OS on Google Compute Engine:
* An account on Google Compute Engine
* The Google Cloud SDK
* The Photon OS image for GCE

Keep in mind that if you try the examples in this document, Google will charge your account for the cloud services that you use. The GCE-ready version of Photon OS, however, comes for free.

#Downloading Photon OS
VMware recommends that administrators use the Photon OS image for Google Compute Engine (GCE) to create Photon OS instances on GCE. Although earlier releases of Photon OS required heavy modification of the Photon instance to make the image compatible with GCE, Photon OS 1.0 comes with pre-modified images for popular cloud providers, including GCE.

You can download the Photon OS image for GCE from the following URL:

[https://bintray.com/vmware/photon](https://bintray.com/vmware/photon)

#Installing Photon OS on Google Compute Engine
After you download the Photon OS image for GCE, create a new bucket to store your Photon OS image for GCE.

![gce1](https://cloud.githubusercontent.com/assets/11306358/9568167/a6a8a91a-4f07-11e5-9a65-8d3867578a3b.jpg)

Once you’ve created the bucket, you’re ready to upload the Photon OS image. While viewing the bucket that you’ve created, click the `Upload files` button, navigate to your Photon OS image and click the `Choose` button. 

As the image below shows, when the upload finishes, you should see your Photon OS compressed image in the file list for the bucket that you created.

![gce2](https://cloud.githubusercontent.com/assets/11306358/9568168/a6c86732-4f07-11e5-8072-1c542d4f12e0.jpg)

Next, you need to create a new image. Click on `Images` in the `Compute` category in the left panel; then, click on the `New Image` button. 

Enter a name for the image in the `Name` field and change the `Source` to `Cloud Storage file` using the pull-down menu. Then, in the `Cloud Storage file` field, enter the bucket name and filename as the path to the Photon OS image for GCE. In this example, where the bucket was named `photon_storage,` the path is as follows:
 
	photon_storage/photon-gce.tar.gz

*Note: The new image form will autopopulate the `gs://` file path prefix.*

After you’re finished completing the name and Cloud Storage object path, click the `Create` button to create your image. You should be returned to your Images catalog and your Photon OS image should be at the top of the list. 

To create an instance, check the box next to the Photon OS image and click the `Create Instance` button. 

On the `Create a new instance` form, provide a name for this instance, confirm the zone into which this instance is to be deployed and, before clicking `Create,` place a check in the boxes for `Allow HTTP traffic` and `Allow HTTPS traffic.` 

Outside of this demo, your application may or may not require these firewall rules. Please choose accordingly outside of this demo. However, since we will demonstrate a running web server container, we want these ports to be open for our instance. 

![gce4](https://cloud.githubusercontent.com/assets/11306358/9568170/a6f30492-4f07-11e5-99b5-a9c2fe72ec1d.jpg)

When the instance is created you will be returned to your list of VM instances. If you click on the instance, the status page for the instance will allow you to SSH into your Photon OS environment using the SSH button at the top of the panel. 

At this point, your instance is running and you are ready to start the Docker engine and run a container workload.

# Installing and Running a Containerized Application

Now that you have your container runtime environment up and running, you may be wondering, “what can I do now?” A command prompt is not the most exciting thing. To help demonstrate the ease in which you can deploy a containerized application, this section showcases how you can quickly get a web server up and running.

This example uses the popular open source web server Nginx. The Nginx application has a customized VMware package and published as a dockerfile and can be downloaded, directly, through the Docker module from the Docker Hub.

To run Docker from the command prompt, enter the command below to initialize the Docker engine:
 
`systemctl start docker`

To ensure Docker daemon service runs on every subsequent VM reboot, enter:
 
`systemctl enable docker`

Now that the Docker daemon service is running, it is a simple task to pull and then start the Nginx Web Server container from Docker Hub.  To do this, type the following command:
 
`docker run -d -p 80:80 vmwarecna/nginx`

This command pulls the Nginx Web Server files and the dependent containers to ensure this containerized application can run.  You will see a screenshot similar to the one below, as the container and dependencies are downloaded and the container is prepared to run:

![gce5](https://cloud.githubusercontent.com/assets/11306358/9568172/a6fe2372-4f07-11e5-82dd-12d6ebd8a7c6.jpg)

Once the `docker run` process is completed, you will be returned to the command prompt.  You now have a fully active website up and running through typing just a single command within Photon OS using containers.

To test that your web server is active, we need to get the IP address of the Photon OS virtual machine. To get the IP address, enter the following command:

	ifconfig 

This command displays a list of network adapters connected to the virtual machine.  Typically, the web server daemon will be bound to `eth0;` record the IP address associated with `eth0.` Alternatively, visit the status page of your Photon OS instance in GCE where the IP address is listed under `External IP.`
 
Start a browser on your host machine and enter the IP address of your Photon OS virtual machine.  The following screen should appear, showing that your web server is active:

![gce6](https://cloud.githubusercontent.com/assets/11306358/9568169/a6d4b140-4f07-11e5-934d-acda0bf505e8.jpg)

You can now run any other containerized application from Docker Hub or your own containerized application within Photon OS.

We hope you enjoy using Photon OS as much as we enjoy creating it.

