# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2017-0010    
Type : Security   
Severity : [ Critical,Moderate]   
Issued on : 2017-Apr-12   
Affected versions : PhotonOS 1.0   

## Details
An update of [binutils,ntp,libarchive] packages for PhotonOS has been released.

## Affected Packages:

### [Critical]
binutils - [CVE-2014-9939](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2014-9939)   
libarchive - [CVE-2016-6250](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-6250)   

### [Moderate]
binutils - [CVE-2017-6969](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6969)  
ntp - [CVE-2017-6451](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6451) [CVE-2017-6452](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6452) [CVE-2017-6455](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6455) [CVE-2017-6462](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6462) [CVE-2017-6463](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6463) [CVE-2017-6464](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6464) [CVE-2017-6458](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6458) [CVE-2017-6460](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-6460)    
libarchive - [CVE-2016-4301](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-4301) [CVE-2016-4300](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-4300) [CVE-2016-4302](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-4302) [CVE-2015-8933](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-8933) [CVE-2016-7166](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-7166) [CVE-2016-4809](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-4809) [CVE-2016-5418](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5418) [CVE-2016-5844](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-5844) [CVE-2016-8687](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8687) [CVE-2016-8688](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8688) [CVE-2016-8689](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-8689) [CVE-2017-5601](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2017-5601)

## Solution
Install the update packages ( _tdnf update package_name_ )

## Updated Packages Information
binutils-2.25.1-4.ph1.x86_64.rpm , sha256 : cb0acbdee89a58207ed6ddb1188a5da895469ed6ac82709b471394145eb7fb89 , size : 4.4M , date : 2017-04-11   
binutils-debuginfo-2.25.1-4.ph1.x86_64.rpm , sha256 : 501c543ab2271859d53224e7e9e3d10b0fdfa1b281f543ac28d5d2f66ae185c0 , size : 7.8M , date : 2017-04-11   
binutils-devel-2.25.1-4.ph1.x86_64.rpm , sha256 : 3177887ecca9db1b096ed7dcf2ad85475f284f5115858820c18c43deabbc0f23 , size : 731K , date : 2017-04-11   
ntp-4.2.8p10-1.ph1.x86_64.rpm , sha256 : 3513797666ecebee0b833dfb09dd723b1f4a5f356160396104e2b3fd43890ba1 , size : 3.5M , date : 2017-04-11   
ntp-debuginfo-4.2.8p10-1.ph1.x86_64.rpm , sha256 : 64f452edc40ec88868829a8312cad19d3faaf1a94c171a8a5b161ff5383e5c0e , size : 3.6M , date : 2017-04-11   
libarchive-3.3.1-1.ph1.x86_64.rpm , sha256 : f7fa908f455099482bf640d3d94239a7ddec6867fbc832822550e525e15e3726 , size : 961K , date : 2017-04-11   
libarchive-debuginfo-3.3.1-1.ph1.x86_64.rpm , sha256 : 873a31763752f5a05d47422cc553fa3b580e8c1d01c4667d658fb33ed4571045 , size : 2.9M , date : 2017-04-11    
libarchive-devel-3.3.1-1.ph1.x86_64.rpm , sha256 : 74db692ef88eb7fc156f52008cc0fa2cfb8f646817d1c4dfbea450b20511b92f , size : 543K , date : 2017-04-11    
