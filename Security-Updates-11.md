# [Critical] Photon OS Security Update


## Summary
Advisory ID: PHSA-2016-0011   
Type : Security   
Severity : [ Critical,Moderate ]   
Issued on : 2016-Nov-29   
Affected versions : PhotonOS 1.0   

## Details
An update of [apache-tomcat,mercurial] packages for PhotonOS has been released.

## Affected Packages:

### [Critical]
apache-tomcat - [CVE-2016-6817](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-6817)    

### [Moderate]
mercurial - [CVE-2016-3068](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-3068) [CVE-2016-3069](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-3069) [CVE-2016-3105](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-3105)      

## Solution
Install the update packages ( tdnf update package_name )

## Updated Packages Information
apache-tomcat-8.5.8-1.ph1.noarch.rpm , sha256 : 2463f8321571dd3ecbd9145aaf3dbdd81b16229f922c0eab08c46dfd2ddac8cb , size : 7.7M , date : 2016-11-29    
mercurial-3.7.1-4.ph1.x86_64.rpm , sha256 : cbe126b4e58ffec8a982537f186bdd5b3e1593f6690404be6708c1b15e7e5127 , size : 8.7M , date : 2016-11-29   
mercurial-debuginfo-3.7.1-4.ph1.x86_64.rpm , sha256 : df546dcded3bdd2c111db5fd87408ba7c879de1555ed628c57bce1fe6c182d47 , size : 121K , date : 2016-11-29   
