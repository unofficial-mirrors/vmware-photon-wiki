# [Moderate] Photon OS Security Update

## Summary
Advisory ID: PHSA-2016-0013    
Type : Security  
Severity : [ Moderate ]  
Issued on : 2016-Dec-07   
Affected versions : PhotonOS 1.0  

## Details
An update of [ subversion, libtasn1, unzip, dhcp ] packages for PhotonOS has been released.

## Affected Packages:

[Moderate]    
subversion - [CVE-2016-2167](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-2167) [CVE-2016-2168](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-2168)  
libtasn1 - [CVE-2016-4008](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-4008)   
unzip - [CVE-2015-7696](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-7696) [CVE-2015-7697](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-7697)   
dhcp - [CVE-2015-8605](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-8605) [CVE-2016-2774](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2016-2774)   

## Solution 
Install the updated packages ( tdnf update _package_name_ )

## Updated packages
subversion-1.9.4-1.ph1.x86_64.rpm , sha256 : bdf294fea2709d2328abb1d4e690985cc730539e31662d0d56a3f7290fad809a , size : 2.7M , date : 2016-12-07   
subversion-debuginfo-1.9.4-1.ph1.x86_64.rpm , sha256 : 60e1ddfbc7f983d7f8be8fb31419e177c54f9af04df1d209a714b995264ae794 , size : 7.4M , date : 2016-12-07   
subversion-devel-1.9.4-1.ph1.x86_64.rpm , sha256 : 1dabfd01d986e4e58daa5961aea12e4f38c88837ac91125b60114c632acfbb4f , size : 347K , date : 2016-12-07   
libtasn1-4.7-3.ph1.x86_64.rpm , sha256 : 7a55db4eca8204cad2ae7d71b9ff13e6c51b5c34706ce3bae054bfd684000bd4 , size : 96K , date : 2016-12-07   
libtasn1-debuginfo-4.7-3.ph1.x86_64.rpm , sha256 : cd6910bbf4bfe005bc39c7ae0a57d8b87c25d10b8f6551a42fb727c7bc199b0d , size : 189K , date : 2016-12-07   
libtasn1-devel-4.7-3.ph1.x86_64.rpm , sha256 : dbe97341d5aa300535fb93cb365563c2cf6ae1d3a85c9d11d493bf75bc4b494f , size : 51K , date : 2016-12-07   
unzip-6.0-7.ph1.x86_64.rpm , sha256 : 5c17c64b1f386fb03b0a865a6547a1bb872e2b333a516e46d69f5359c0cdc24f , size : 119K , date : 2016-12-07   
unzip-debuginfo-6.0-7.ph1.x86_64.rpm , sha256 : cde0ffcb05db6626d22048d7fa908f4d860ce0a83e5b34537ba7ff2c1f97c862 , size : 2.4K , date : 2016-12-07   
dhcp-client-4.3.5-1.ph1.x86_64.rpm , sha256 : ea503fa583015108352d3a06f98219a0b1b0bb81d91634ae5cfbee13b4aff84a , size : 862K , date : 2016-12-07   
dhcp-debuginfo-4.3.5-1.ph1.x86_64.rpm , sha256 : ed24bf5de5b38b9e2ae230d5f472018e0df3fdd98f061a60a5f0fb769ac5c4d2 , size : 7.3M , date : 2016-12-07   
dhcp-devel-4.3.5-1.ph1.x86_64.rpm , sha256 : 3e604a2ee1cbf09ad3ed84a542504720eb2d4fd322ece8b64c5c695d790ef384 , size : 17K , date : 2016-12-07   
dhcp-libs-4.3.5-1.ph1.x86_64.rpm , sha256 : d1789b1101984e62f2bdfb2ba822913e94310f02b345370686dfee7bf45414a3 , size : 63K , date : 2016-12-07   
dhcp-server-4.3.5-1.ph1.x86_64.rpm , sha256 : e974d0ea489717c1816ea02420724deacbd0df8ea267872dfee618880ed28556 , size : 2.6M , date : 2016-12-07